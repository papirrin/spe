/*    
 *   Robert Lim
 *   roblim1@ics.uci.edu
 */

//#define _GNU_SOURCE
#include <linux/perf_event.h>

#define PERF_MULTIPLEX_DEBUG_ALL

//
//  multiplex.c classes
//
struct last_three_points {
  /* stores last three observations (time or count) */
  unsigned long long A;
  unsigned long long B;
  unsigned long long C;
};

struct perf_multiplex_data {
  unsigned long long d_time[2];   //  x(t-1), x(t)
  unsigned long long d_count[2];  //  y(t-1), y(t)
  struct last_three_points ltp_time;
  struct last_three_points ltp_count;
};

//
// Light-weight perf_event classes for testing purposes
//
struct perf_event_light {
  /* light weight version of perf_event */
  struct perf_event_attr attr; /* user space settings */
  struct perf_multiplex_data *multiplex_data; /* our chit */

};

struct list_pe_light {
  /* linked list of perf_event_light */
  struct perf_event_light *event;
  struct list_pe_light *next;
};

typedef struct list_pe_light node;

void perf_multiplex_init(struct perf_event_light *event);
void pe_light_print_list(node *ll);

static struct perf_event_light*
perf_event_light_alloc(struct perf_event_attr *attr)
{
  struct perf_event_light *event;
  
  #ifdef PERF_MULTIPLEX_DEBUG_ALL
    printf("^^^ BEGIN: perf_event_light_alloc\n");
  #endif
  
  event = malloc(sizeof(struct perf_event_light));
  if(!event) {
    printf("^^^ -- failed malloc inside perf_event_light_alloc\n");
    exit(1);
  }
  
  event->attr = *attr;
  perf_multiplex_init(event);
  
  #ifdef PERF_MULTIPLEX_DEBUG_ALL
    printf("^^^ END: perf_event_light_alloc\n");
  #endif

  /* insert into existing list */
  return event;
}


void perf_multiplex_init(struct perf_event_light *event)
{
  // Already initialized
  if (event->multiplex_data != NULL) {
    printf("@@@ -- event->multiplex already initialized\n");
    return;
  }
  
  // Multiplex not set
  if (!event->attr.multiplex) {
    printf("@@@ -- event->attr.multiplex not set\n");
    return;
  }
  
  #ifdef PERF_MULTIPLEX_DEBUG_ALL
    printf("@@@ BEGIN perf_multiplex_init\n");
  #endif
  
  event->multiplex_data = malloc(sizeof(struct perf_multiplex_data));
  if (NULL == event->multiplex_data) {
    printf("-- perf_multiplex_init failed to allocate perf_multiplex_data.\n");
  }
  
  #ifdef PERF_MULTIPLEX_DEBUG_ALL
    printf("@@@ END: perf_multiplex_init\n");
  #endif

}

void perf_multiplex_terminate(struct perf_event_light *event)
{
  #ifdef PEFF_MULTIPLEX_DEBUG_ALL
    printk("@@@ Inside perf_multiplex_terminate().\n");
  #endif
  
  if (event->multiplex_data) {
    free(event->multiplex_data);
    event->multiplex_data = NULL;
  }
}


void perf_multiplex_update(struct perf_event_light *event, 
			   int restart_recent_count,
			   unsigned long long curr_count, 
			   unsigned long long curr_time)
{

  struct perf_multiplex_data* m_data = event->multiplex_data;

  #ifdef PERF_MULTIPLEX_DEBUG_ALL
    printf("## Begin: perf_multiplex_update ##\n");
  #endif

  if (!m_data) {
    if (event->attr.multiplex)
      printf("-- perf_multiplex_update, null ref to perf_multiplex_data.\n");
    return;
  }
  
  // move b<-c; a<-b; c<-(&event->count);
  m_data->ltp_count.A = m_data->ltp_count.B;
  m_data->ltp_count.B = m_data->ltp_count.C;
  m_data->ltp_count.C = curr_count;
  /* Do the same for time!!! */
  m_data->ltp_time.A = m_data->ltp_time.B;
  m_data->ltp_time.B = m_data->ltp_time.C;
  m_data->ltp_time.C = curr_time;
  
  #ifdef PERF_MULTIPLEX_DEBUG_ALL
    printf("-- time.A: %llu, time.B: %llu, time.C: %llu\n",
	   m_data->ltp_time.A, m_data->ltp_time.B, 
	   m_data->ltp_time.C);
    printf("-- count.A: %llu, count.B: %llu, count.C: %llu\n",
	   m_data->ltp_count.A, m_data->ltp_count.B, 
	   m_data->ltp_count.C);
  #endif

  #ifdef PERF_MULTIPLEX_DEBUG_ALL
    printf("## End: perf_multiplex_update ##\n");
  #endif

}

/*
  calculate_deltas(): perf_event* -> void
  Calculates delta pairs for the last three observed points
  (count, time)
*/
void perf_multiplex_calculate_deltas(struct perf_event_light* event)
{
  if (!event->attr.multiplex)
    return;

  struct perf_multiplex_data* m_data = event->multiplex_data;
  // failed creation
  if (!m_data)
    return;

  #ifdef PERF_MULTIPLEX_DEBUG_ALL
    printf("vv Begin: perf_multiplex_calculate_deltas ##\n");
  #endif

  // base case (when A = 0);
  // base case (when B = 0);

  /* assuming A, B, C already have values.. */  
  m_data->d_count[1] = m_data->ltp_count.C - m_data->ltp_count.B;
  m_data->d_count[0] = m_data->ltp_count.B - m_data->ltp_count.A;
  m_data->d_time[1] = m_data->ltp_time.C - m_data->ltp_time.B;
  m_data->d_time[0] = m_data->ltp_time.B - m_data->ltp_time.A;

  #ifdef PERF_MULTIPLEX_DEBUG_ALL
    printf("-- d_time[0]: %llu, d_time[1]: %llu\n",
	   m_data->d_time[0], m_data->d_time[1]);
    printf("-- d_count[0]: %llu, d_count[1]: %llu\n",
	   m_data->d_count[0], m_data->d_count[1]);
  #endif

  #ifdef PERF_MULTIPLEX_DEBUG_ALL
    printf("vv End: perf_multiplex_calculate_deltas ##\n");
  #endif

}


struct perf_event_light *
perf_multiplex_get_low_var_event(node *ll)
{
  //printf("** Printing list from perf_multiplex_get_low_var_event()\n");
  //pe_light_print_list(ll);
  
  struct perf_event_light *event_ret = NULL;
  
  // go through each event and do triangle calculation..
  node *current = ll;
  int i = 0, event_i = 0;
  unsigned long long prev_cost = 0;

  // traverse event..
  while (current != NULL) {
    
    // points
    unsigned long long Ax, Bx, Cx, Ay, By, Cy; // x = time, y = count 
    
    // triangles
    unsigned long long ABD, BCD;
    unsigned long long ABDCost, BCDCost;

    // equation
    unsigned long long delta_y;

    Ax = current->event->multiplex_data->ltp_time.A;
    Bx = current->event->multiplex_data->ltp_time.B;
    Cx = current->event->multiplex_data->ltp_time.C;
    Ay = current->event->multiplex_data->ltp_count.A;
    By = current->event->multiplex_data->ltp_count.B;
    Cy = current->event->multiplex_data->ltp_count.C;
    delta_y = ((Cy-Ay)/(Cx-Ax)) * (Bx-Ax);

    // triangle calc
    ABD     = abs( ((Bx-Ax)/2) * (By-Ay-delta_y) );
    ABDCost = (By-Ay-delta_y) / 2;
    BCD     = abs( ((Cx-Bx)/2) * (By-Ay-delta_y) );
    BCDCost = (By-Ay-delta_y) / 2;

    #ifdef PERF_MULTIPLEX_DEBUG_ALL
      printf("<<< i: %i\n", ++i);
      printf("<<< ABD: %llu, ABDCost: %llu\n", ABD, ABDCost);
      printf("<<< BCD: %llu, BCDCost: %llu\n", BCD, BCDCost);
    #endif
    
    // keep track of who's lowest so far, then return that event
    // first timer, or if current cost is greater than prev_cost
    if ( !event_ret || (ABDCost > prev_cost) ) {
      event_i = i;
      event_ret = current->event;
      prev_cost = ABDCost;
    }
    // else, do nothing (stored variance is highest so far)..
    current = current->next;
  }

  printf("<<< event_i: %i, prev_cost: %llu\n", event_i, prev_cost);

  // head of class
  return event_ret;
}
							  
/*
  Helper functions for building linked lists
*/

int pe_light_insert_node(node **ll, 
			 struct perf_event_light *event)
{
  
  node* ll_new = (node *)(malloc(sizeof(node)));
  
  if(!ll_new) {
    printf("Failed malloc node\n");
    exit(1);
  }
  
  ll_new->event = event;
  ll_new->next = NULL;
  
  if(*ll == NULL)
    *ll = ll_new;
  else {
    ll_new->next = *ll;
    *ll = ll_new;
  }
  
  return 1;
}

node *pe_light_rotate_left(node *head) {
  // check for empty list!
  node *oldHead = head;
  head = head->next;
  
  // append old head to end of list: find tail -> add old head after tail
  node *tail = head;
  while (tail->next != NULL)
    tail = tail->next;
  
  // add after tail
  tail->next = oldHead;
  oldHead->next = NULL;

  return head;
}

void pe_light_print_list(node *ll)
{

  node *current = ll;
  int i = 0;
  while (current != NULL) {
    printf("<<< i: %i, d_count[0]: %llu\n", ++i, 
	   current->event->multiplex_data->d_count[0]);
    current = current->next;
  }
  
}

void pe_light_build_list(struct perf_event_attr pe, 
			 int **fd, long long **events, 
			 node **ll, 
			 int NUM_EVENTS, int NUM_EVENTSETS)
{
  int i, j;
  
  #ifdef PERF_MULTIPLEX_DEBUG_ALL
    printf("=== BEGIN: pe_light_build_list\n");
  #endif

  /* building perf_event structs.. */
  for(j=0; j<NUM_EVENTSETS; j++) {
    fd[j][0] = -1;
    for(i=0; i<NUM_EVENTS; i++) {
      memset(&pe, 0, sizeof(struct perf_event_attr));
      pe.type = PERF_TYPE_HARDWARE;
      pe.size = sizeof(struct perf_event_attr);
      pe.config = events[j][i];
      if (i==0) {
	pe.read_format=PERF_FORMAT_GROUP|PERF_FORMAT_ID|
	  PERF_FORMAT_TOTAL_TIME_ENABLED|
	  PERF_FORMAT_TOTAL_TIME_RUNNING;
	pe.disabled = 1;
      }
      pe.exclude_kernel = 1;
      pe.exclude_hv = 1;
      pe.multiplex = 1;
      pe.freq = 0;
      pe.sample_period = 1000;
      /*pe.adaptive=1;*/

      struct perf_event_light *event;
      event = perf_event_light_alloc(&pe);
      if (NULL == event) {
	printf("=== -- failed to allocate perf_event_light\n");
	return;
      }
      // insert event into list
      if(!pe_light_insert_node(ll, event)) {
	printf("=== -- failed inserting node\n");
	exit(1);
      }

    } // i
  } // j
  
  #ifdef PERF_MULTIPLEX_DEBUG_ALL
    printf("=== END: pe_light_build_list\n");
  #endif
}
