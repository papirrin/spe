/*    
 *   Robert Lim
 *   roblim1@ics.uci.edu
 */

#define _GNU_SOURCE

//#define NUM_OBSERVATIONS 385  /* perf_sample_data.txt */
#define NUM_OBSERVATIONS 5  /* perf_sample_data_sm.txt */


/* imported data */
struct raw_data {
  unsigned long long time[NUM_OBSERVATIONS];
  unsigned long long count[NUM_OBSERVATIONS];
};

void raw_data_print(struct raw_data *data, int numEvents)
{  
  if (!data) {
    printf("+++ -- inside raw_data_print, raw_data = NULL\n");
    exit(1);
  }

  int i, j;

  #ifdef PERF_MULTIPLEX_DEBUG_ALL
    printf("+++ raw_data_print()\n");
  #endif

  for (i=0; i<numEvents; i++) {
    for (j=0; j<NUM_OBSERVATIONS; j++) {
      printf("+++ data[%i].time[%i]: %llu, data[%i].count[%i]: %llu\n",
	     i, j, data[i].time[j], i, j, data[i].count[j]);
    }
  }
 
}

void raw_data_import(struct raw_data *data, const char *fname) 
{
  if (!data) {
    printf("$$$ -- raw_data = NULL, inside raw_data_import\n");
    exit(1);
  }

  FILE *fp;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  int col = 0, row = 0;

  #ifdef PERF_MULTIPLEX_DEBUG_ALL
    printf("$$$ BEGIN: raw_data_import\n");
  #endif

  fp = fopen(fname, "r");
  if (NULL == fp) {
    printf("$$$ -- fopen failed!\n");
    exit(1);
  }

  while ((read = getline(&line, &len, fp)) != -1) {
    const char delim[2] = ",";
    char *token;
    char *pEnd;

    token = strtok(line, delim);
    int group = 0;
    
    while (token != NULL) {
      if (col%2) {
	data[group].time[row] = strtoull(token, &pEnd, 10); 
	group++;
      }
      else
	data[group].count[row] = strtoull(token, &pEnd, 10);    
      
      token = strtok(NULL, delim);
      col++; // determine if we're reading counts or time..
    }// while token    
    
    row++;
  }// while line
  
  // release
  if(line)
    free(line);

  #ifdef PERF_MULTIPLEX_DEBUG_ALL
    printf("$$$ END: raw_data_import\n");
  #endif
  
}
