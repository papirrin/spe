/*    
 *   Robert Lim
 *   roblim1@ics.uci.edu
 */

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <linux/perf_event.h>

#include "multiplex.c"
#include "multiplextest.c"

#define NUM_EVENTS       2
#define NUM_EVENTSETS    3

typedef struct list_pe_light node;

int main(int argc, char **argv)
{

  int i, j;
  //const char fname[] = "perf_data_sample.txt";
  const char fname[] = "perf_data_sample_sm.txt";
  struct perf_event_attr pe;
  struct raw_data *data;
  node *ll = NULL;  // store all perf_event in linked-list
  int **fd;
  long long **events;

  fd = malloc(sizeof(int *) * NUM_EVENTSETS);
  if (!fd)
    printf("malloc dp for fd failed\n");
  for (i=0; i<NUM_EVENTSETS; i++) {
    fd[i] = malloc(sizeof(int) * NUM_EVENTS);
    if (!fd[i])
      printf("malloc failed inside loop at iteration: %i\n", i);
  }

  events = malloc(sizeof(long long*) * NUM_EVENTSETS);
  if (!events)
    printf("malloc dp for events failed\n");
  for (i=0; i<NUM_EVENTSETS; i++) {
    events[i] = malloc(sizeof(long long) * NUM_EVENTS);
    if (!events[i])
      printf("malloc failed for events[%i]\n", i);
  }

  // group0
  events[0][0] = PERF_COUNT_HW_CPU_CYCLES;
  events[0][1] = PERF_COUNT_HW_INSTRUCTIONS;
  // group1
  events[1][0] = PERF_COUNT_HW_STALLED_CYCLES_FRONTEND;
  events[1][1] = PERF_COUNT_HW_REF_CPU_CYCLES;
  // group2
  events[2][0] = PERF_COUNT_HW_REF_CPU_CYCLES;
  events[2][1] = PERF_COUNT_HW_BRANCH_MISSES;

  #ifdef PERF_MULTIPLEX_DEBUG_ALL
    printf("$$$ BEGIN: main\n");
  #endif
    
  /* build linked-list of perf events */
  pe_light_build_list(pe, fd, events, &ll, NUM_EVENTS, NUM_EVENTSETS);
  //pe_light_print_list(ll);

  /* import test data */
  data = (struct raw_data*)malloc(sizeof(struct raw_data)*
				  (NUM_EVENTSETS*NUM_EVENTS));
  if(!data) {
    printf("$$$ -- raw_data malloc failed.\n");
    exit(1);
  }

  raw_data_import(data, fname);
  raw_data_print(data, NUM_EVENTSETS*NUM_EVENTS);
  
  #ifdef PERF_MULTIPLEX_DEBUG_ALL
    printf("$$$ --- import_test_data() OK\n");
  #endif
  
  /* simulate sched_tick() and rotate_ctx() */
  for (j=0; j<NUM_OBSERVATIONS; j++)
  {
    // sched_tick()
    printf("$$$ --- Snoozing ...\n");
    sleep(1);
    // 1. Copy counter values to 'struct last_three_points'
    // 2. Call perf_multiplex_calculate_deltas
    // 3. Rearrange event list according to deltas
    // ** (1) would normally occur in __perf_event_overflow and/or 
    //    perf_freq_adjust_unthr_context, but lumping together for 
    //    testing purposes.
    
    int n = 0;
    node *ll_temp = ll;  // event list

    // 1. Read counts
    while (ll_temp) {
      
      unsigned long long curr_count, curr_time;
      curr_count = data[n].count[j];
      curr_time = data[n].time[j];

      printf("$$$ -- Event (%i):  \n", ++n);
      perf_multiplex_update(ll_temp->event, 0, curr_count, curr_time);

      ll_temp = ll_temp->next;
    }

    ll_temp = ll;

    // 2. Calculate deltas
    perf_multiplex_calculate_deltas(ll_temp->event);
    
    // Beware! Incompatible pointer type error (c99, pe_light local scope)
    struct perf_event_light *event_to_sched_next = 
      perf_multiplex_get_low_var_event(ll_temp);  // lowest variance of all..
      
    if (!event_to_sched_next)
      printf("$$$ perf_multiplex_get_low_var_event failed!\n");
    
    // 3. Rotate list until target is being pointed at
    ///////////////////////////////////////////////////////////
    //    call rotate_ctx() until perf_event matches up!     //
    ///////////////////////////////////////////////////////////
    //node *ll_temp2 = pe_light_rotate_left(ll);
    //if(!ll_temp2)
    //printf("$$$ failed rotate_left\n");
  
  } // for j

  #ifdef PERF_MULTIPLEX_DEBUG_ALL
    printf("$$$ END: main\n");
  #endif
  
  return 0;

} // main

