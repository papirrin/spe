#define _GNU_SOURCE 1
#include <stdio.h>
#include <assert.h>

#include "spe_internal.h"
#include "utils.h"
#include "record/spe_record.h"
#include "parse/spe_parse.h"

#include <sysexits.h>
#include <sys/stat.h>
#include <string.h>
#include <assert.h>

int
main(int argc, char **argv)
{
  // TODO: Whait is an acceptable minimum? It depends of each event
  const int PERIOD_MIN = 1;
  char usage[1600] ="\
Usage: \t spe [options] command [command_options]\n"
	"\t-p <f:period-evN,f:period-evN,...><r:seed1:min1:max1,r:seed2:min2:max2>"
		"\tObligatory as of know. Seed, min and max of a uniform distribution for the period or a fixed period\n"
	"\t-a monitor_cpu_mask:load_cpu_mask"
		"\tTwo comma lists of cpu id's that will be converted into masks for sched_setaffinity.\n"
	"\t-c Integer index of dessired configuration settings.\n"
	"\t-v\t"
		"\t\tBe verbose.\n"
	"\t-o <output_preffix>"
		"\tPreffix to prepend to all outputs.\n"
	"\t-d\t"
		"\t\tDirectory to generate output on.\n"
	"\t-h\t"
		"\t\tThis help.\n";

  struct spe_record__conf r_conf;
  int i;
  CPU_SET(0,&r_conf.record_aff_mask);
  CPU_SET(0,&r_conf.load_aff_mask);
  //r_conf.evs_conf = SPC_CTL_MULTIPLEX4;
  strcpy(r_conf.output_preffix, "test");

  for(i=0; i < PERF_EVENT_MAX_EVENTS; ++i){
	  r_conf.period[i].fixed_period = 5000000;
  }
  r_conf.verbose = 0;
  
  struct spe_parse__conf p_conf;
  
  char data_dir[FILENAME_MAX];
  strcpy(data_dir,"data");

  int cmd_idx = 1;
  int nr_period_sets = 0;

  char arg_mask_mon[FILENAME_MAX], arg_mask_load[FILENAME_MAX];

  char * aux_char = NULL;
  // TODO: Sanitize input for missing arguments in options that require one
  while (cmd_idx < argc && argv[cmd_idx][0] == '-')
    {
      switch (argv[cmd_idx++][1])
        {
      case 'v':
        r_conf.verbose = 1;
        break;
      case 'c':
	r_conf.evs_conf =atoi(argv[cmd_idx++]);
	if(r_conf.evs_conf < 0 || r_conf.evs_conf >= SPC_CTL_MAX_NUMBER){
		goto error_c_tag;
	}
	break;
      case 'p':

	if (cmd_idx + 1 >= argc) goto error_p_tag;

	aux_char = strtok(argv[cmd_idx++], ",:");
	printf("%s", aux_char);
	while(aux_char){
		switch(aux_char[0]){
			case 'f':
				r_conf.period[nr_period_sets].fixed_period = strtoul(strtok(NULL,",:"), NULL, 10);
			        if (r_conf.period[nr_period_sets].fixed_period < PERIOD_MIN)
          		        	printf("-p period must be larger or equal than %d.\n", PERIOD_MIN);
			break;
			case 'r':
				r_conf.period[nr_period_sets].fixed_period = 0;
			        r_conf.period[nr_period_sets].seed = atoi(strtok(NULL, ",:"));
			        r_conf.period[nr_period_sets].min_period = strtoul(strtok(NULL, ",:"), NULL, 10);
			        r_conf.period[nr_period_sets].max_period = strtoul(strtok(NULL, ",:"), NULL, 10);

			break;
			default: goto error_p_tag;
		}
		aux_char = strtok(NULL, ",:");
	        ++nr_period_sets;
	}
	break;
      case 'o':
        strcpy(r_conf.output_preffix, argv[cmd_idx++]);
        break;
      case 'd':
        strcpy(data_dir, argv[cmd_idx++]);
        int dd_len = strlen(data_dir);
        if(dd_len > 0 && data_dir[strlen(data_dir)-1] == '/'){
        	data_dir[strlen(data_dir)-1] = '\0';
        }
        break;
      case 'a':
	if(cmd_idx + 1 >= argc) goto error_a_tag;

        aux_char = strtok(argv[cmd_idx++],":");
        if(aux_char == NULL) goto error_a_tag;
        strcpy(arg_mask_mon, aux_char);

        aux_char = strtok(NULL,":");
        if(aux_char == NULL) goto error_a_tag;
        strcpy(arg_mask_load, aux_char);

        set_aff(arg_mask_mon, &r_conf.record_aff_mask);
        set_aff(arg_mask_load, &r_conf.load_aff_mask);
        break;
  error_c_tag:
        fprintf(stderr, "Invalid format for flag -c %s\n", argv[cmd_idx-1]);
        exit(-EX_USAGE);

  error_p_tag:
        fprintf(stderr, "Invalid format for flag -p %s\n", argv[cmd_idx-1]);
        exit(-EX_USAGE);

  error_a_tag:
        fprintf(stderr, "Invalid format for flag -a %s\n", argv[cmd_idx-1]);
        exit(-EX_USAGE);
        
      case 'h':
        printf("%s\n", usage);
        exit(0);
      default:
        printf("Invalid option: %s.\n\n. Try spe -h\n", argv[cmd_idx - 1]);
        exit(-EX_USAGE);
        }
    }
  r_conf.nr_events = nr_period_sets;
  
  //create_dir(data_dir);
  _mkdir(data_dir);
  strcpy(r_conf.output_path,data_dir);
  strcpy(p_conf.input_path, data_dir);

  set_summary_output_full_fn(
    r_conf.output_path, r_conf.output_preffix, p_conf.input_summ_fn);
  sprintf(p_conf.output_preffix, "%s_ev", r_conf.output_preffix);

  /*
  * Handle load command and its arguments
  */
  int nr_load_args = argc - cmd_idx;
  if (nr_load_args < 1)
  {
    printf("Error: A command must be specified.\n\n%s\n", usage);
    exit(-1);
  }
  else{
    char *fill_line = "+++++++++++++++++++++++++++++\n";
    printf("%sLoad to run: ",fill_line);
    int i=0;
    for(i=cmd_idx; i < argc; ++i){
      printf("%s ",argv[i]);
    }
    printf("\n%s", fill_line);
  }

  // Copy load command and arguments
  char ** load_args = malloc(sizeof(char*) * (nr_load_args + 1));
  for(i =0; i < nr_load_args; ++i)
    {
      load_args[i] = malloc(sizeof(char)*(strlen(argv[cmd_idx+i])+1));
      strcpy(load_args[i], argv[cmd_idx + i]);
    }
  load_args[nr_load_args] = NULL;

  r_conf.wl_args = load_args;

  printf("Recording started ...\n");
  spe_record__record(&r_conf);
  printf("Recording ended. Parsing ...\n");
  spe_parse__parse(&p_conf);
  
  for (i=0; i < nr_load_args; ++i)
    {
      free(load_args[i]);
    }
  free(load_args);
  printf("Finished.\n");
  return 0;
}
