#define _GNU_SOURCE 1
#include <stdio.h>
#include <assert.h>
#include <sysexits.h>
#include <sys/stat.h>
#include <string.h>
#include <assert.h>
#include <sched.h>

static void _mkdir(const char *dir) {
  char tmp[256];
  char *p = NULL;
  size_t len;
 
  snprintf(tmp, sizeof(tmp),"%s",dir);
  len = strlen(tmp);
  if(tmp[len - 1] == '/')
    tmp[len - 1] = 0;
  for(p = tmp + 1; *p; p++)
    if(*p == '/') {
      *p = 0;
      mkdir(tmp, S_IRWXU);
      *p = '/';
    }
  mkdir(tmp, S_IRWXU);
}

void create_dir(char * dir_path){
          struct stat sb;
          if(stat(dir_path, &sb)==0){
            if(!S_ISDIR(sb.st_mode)){
              REPORTERROR("A file with the name of the data directory,%s, already exists\n", dir_path);
              exit(-1);
            }
          }
          else{
            int ret=mkdir(dir_path, S_IRWXU | S_IRWXG | S_IROTH);
            check_notneg(ret, "Error creating data directory. Error:%d\n", ret);
          }
}


int set_aff(char * mask_str, cpu_set_t * mask ){
  assert(mask);
  if(mask_str == NULL || strlen(mask_str) <= 0){
      sched_getaffinity(0, sizeof(cpu_set_t), mask);
      return 0;
  }
  else{
      CPU_ZERO(mask);
  }
  char * next = strtok(mask_str, ",");

  while(next!= NULL){
      CPU_SET(atoi(next), mask);
      next = strtok(NULL, ",");
  }
  return 0;
}

