#include "spe_internal.h"

void REPORTERROR(const char *format, ...){
  va_list argptr;
  va_start(argptr, format);
  vfprintf(stderr, format, argptr);
  va_end(argptr);
}

void SUBDBG(const char *format, ...){
  va_list argptr;
  va_start(argptr, format);
  vprintf(format, argptr);
  va_end(argptr);
}


int sys_perf_event_open(struct perf_event_attr *attr,
                    pid_t pid, int cpu, int group_fd,
                    unsigned long flags){
  SUBDBG("perf_event_open call with attrs(type: %d, size: %d), on PID: %d, CPU: %d, Group_fd: %d, Flags: %d\n", attr->type, attr->size,
pid, cpu, group_fd, flags);
  return syscall(__NR_perf_event_open, attr, pid, cpu, group_fd, flags);
}

void check_notnull(void * ret, const char * format, ...){
  va_list argptr;
    if (ret == NULL) {
            va_start(argptr, format);
            vfprintf(stderr, format, argptr);
            va_end(argptr);
            exit(EXIT_FAILURE);
    }
}

void check_notneg(int ret, const char * format, ...){
  va_list argptr;
    if (ret < 0) {
            va_start(argptr, format);
            vfprintf(stderr, format, argptr);
            va_end(argptr);
            exit(EXIT_FAILURE);
    }
}
void check_return(int ret, const char * format, ...) {
        va_list argptr;
        if (ret != PE_OK) {
                va_start(argptr, format);
                vfprintf(stderr, format, argptr);
                va_end(argptr);
                exit(EXIT_FAILURE);
        }
}

void event_bin_output_full_fn(char * path, char * preffix, int group, int event, char * o_buff){
  sprintf(o_buff, "%s/%s_%d_%d.bin", path, preffix, group, event);
}

void set_summary_output_full_fn(char * path, char * preffix, char * o_buff){
  sprintf(o_buff, "%s/%s_summary.csv", path, preffix);
}
