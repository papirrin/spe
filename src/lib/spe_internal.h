#ifndef _SPE_INTERNAL_H
#define _SPE_INTERNAL_H

#define _GNU_SOURCE 1

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <linux/perf_event.h>
#include <sys/syscall.h>


#define PERF_EVENT_MAX_MPX_COUNTERS 20
#define PERF_EVENT_MAX_GROUPS 8
#define PERF_EVENT_MAX_EVENTS PERF_EVENT_MAX_GROUPS * PERF_EVENT_MAX_MPX_COUNTERS

#define TEXT_BUFFS_SIZE 1000

//#define PE_SAMPLE_SIG SIGUSR1
#define PE_SAMPLE_SIG SIGIO

#define PE_OK          0     /**< No error */
#define PE_EINVAL     -1     /**< Invalid argument */
#define PE_ENOMEM     -2     /**< Insufficient memory */
#define PE_ESYS       -3     /**< A System/C library call failed */
#define PE_ECMP       -4     /**< Not supported by component */
#define PE_ESBSTR     -4     /**< Backwards compatibility */
#define PE_ECLOST     -5     /**< Access to the counters was lost or interrupted */
#define PE_EBUG       -6     /**< Internal error, please send mail to the developers */
#define PE_ENOEVNT    -7     /**< Event does not exist */
#define PE_ECNFLCT    -8     /**< Event exists, but cannot be counted due to counter resource limitations */
#define PE_ENOTRUN    -9     /**< EventSet is currently not running */
#define PE_EISRUN     -10    /**< EventSet is currently counting */
#define PE_ENOEVST    -11    /**< No such EventSet Available */
#define PE_ENOTPRESET -12    /**< Event in argument is not a valid preset */
#define PE_ENOCNTR    -13    /**< Hardware does not support performance counters */
#define PE_EMISC      -14    /**< Unknown error code */
#define PE_EPERM      -15    /**< Permission level does not permit operation */
#define PE_ENOINIT    -16    /**< PAPI hasn't been initialized yet */
#define PE_ENOCMP     -17    /**< Component Index isn't set */
#define PE_ENOSUPP    -18    /**< Not supported */
#define PE_ENOIMPL    -19    /**< Not implemented */
#define PE_EBUF       -20    /**< Buffer size exceeded */
#define PE_EINVAL_DOM -21    /**< EventSet domain is not supported for the operation */
#define PE_EATTR              -22    /**< Invalid or missing event attributes */
#define PE_ECOUNT             -23    /**< Too many events or attributes */
#define PE_ECOMBO             -24    /**< Bad combination of features */
#define PE_NUM_ERRORS  25    /**< Number of error messages specified in this API */

void REPORTERROR(const char *format, ...);

void SUBDBG(const char *format, ...);

int sys_perf_event_open(struct perf_event_attr *attr,
                    pid_t pid, int cpu, int group_fd,
                    unsigned long flags);
void check_notnull(void * ret, const char * format, ...);
void check_notneg(int ret, const char * format, ...);
void check_return(int ret, const char * format, ...);

void event_bin_output_full_fn(char * path, char * preffix, int group, int event, char * o_buff);
void set_summary_output_full_fn(char * path, char * preffix, char * o_buff);
#endif
