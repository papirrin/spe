/*
 * event_conf.h
 *
 *  Created on: Apr 5, 2013
 *      Author: papirrin
 */

#ifndef EVENT_CONF_H_
#define EVENT_CONF_H_

#include "event_conf_types.h"
#include "spe_sampling.h"

int initialize_groups(pe_groups_t * groups, const struct spe_record__conf * conf, int pid);


#endif /* EVENT_CONF_H_ */
