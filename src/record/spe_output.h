/*
 * pe_output.h
 *
 *  Created on: Mar 25, 2013
 *      Author: papirrin
 */

#ifndef SPE_OUTPUT_H_
#define SPE_OUTPUT_H_

#include "spe_sampling.h"
#include <inttypes.h>

void
init_output(pe_output_t * o)
{
  memset(&o->count, 0, sizeof(pe_count_t));
  o->output_fp = NULL;
  //o->output_style = OUT_STYLE_HREADABLE;
}

void
add_count(pe_output_t * o, int si_code)
{
  pe_count_t * c = &o->count;
  switch (si_code)
    {
  case POLL_IN:
    c->in++;
    break;
  case POLL_OUT:
    c->out++;
    break;
  case POLL_MSG:
    c->msg++;
    break;
  case POLL_ERR:
    c->err++;
    break;
  case POLL_PRI:
    c->pri++;
    break;
  case POLL_HUP:
    c->hup++;
    break;
  default:
    c->unknown++;
    break;
    }

  c->total++;
}

int
open_outputs(pe_groups_t *g, char * path, char * preffix)
{

  //printf("\n\nBuffer size %d\n", BUFSIZ);
  assert(g);
  assert(preffix);

  int i, j;
  FILE* fp;
  char buff[TEXT_BUFFS_SIZE];
  for (i = 0; i < g->num_groups; ++i)
    {
      pe_control_t *c = &g->groups[i];
      for (j = 0; j < c->num_events; ++j)
        {
    	  c->events[j].output.nr_overflows=0;
          event_bin_output_full_fn(path, preffix, i, j, buff);
          fp = fopen(buff, "w");
          if (fp == NULL)
            {
              return -1;
            }
          setbuffer(fp, c->events[j].output.output_buf, OUTPUT_BUFF_SIZE);
          c->events[j].output.output_fp = fp;
        }
    }
  return 0;
}

int
close_outputs(pe_groups_t *g)
{
  int i, j;
  int failures = 0;

  for (i = 0; i < g->num_groups; ++i)
    {
      pe_control_t *c = &g->groups[i];
      for (j = 0; j < c->num_events; ++j)
        {
          if (fclose(c->events[j].output.output_fp) < 0)
            {
              ++failures;
            }
        }
    }
  return -failures;
}

int
print_summaries(pe_groups_t *g, char * path, char * preffix)
{
  char buff[2 * FILENAME_MAX];
  FILE * f_summary;

  set_summary_output_full_fn(path, preffix, buff);
  f_summary = fopen(buff, "w");

  char * summary_header = "g_idx,e_idx,filename, in,out,msg,err,pri,hup,unk,tot,";
  fprintf(f_summary, "%s", summary_header);


  char * atts_header = "event_type,config,init_sample_period,sample_type,read_format,inherit," \
      "pinned,precise_ip,config1,config2,nr_overflows,final_count,time_enabled,time_sampling\n";
  fprintf(f_summary, "%s", atts_header);
  /* Counts */
  long int tot_signs = 0;
  unsigned int tot_overflows = 0;
  int g_idx, e_idx;
  for (g_idx = 0; g_idx < g->num_groups; ++g_idx)
    {
      pe_control_t *c = &g->groups[g_idx];
      for (e_idx = 0; e_idx < c->num_events; ++e_idx)
        {

          pe_output_t * o = &c->events[e_idx].output;
          printf("\nGroup: %d, Event: %d, Overflows: %d\n", g_idx, e_idx,o->nr_overflows);
          printf("\tPOLL_IN : %d\n", o->count.in);
          printf("\tPOLL_OUT: %d\n", o->count.out);
          printf("\tPOLL_MSG: %d\n", o->count.msg);
          printf("\tPOLL_ERR: %d\n", o->count.err);
          printf("\tPOLL_PRI: %d\n", o->count.pri);
          printf("\tPOLL_HUP: %d\n", o->count.hup);
          printf("\tUNKNOWN : %d\n", o->count.unknown);

	  event_bin_output_full_fn(path, preffix, g_idx,e_idx, buff);
          fprintf(f_summary, "%d,%d,%s,%d,%d,%d,%d,%d,%d,%d,%d,",
              g_idx, e_idx, buff,
              o->count.in, o->count.out, o->count.msg,
              o->count.err, o->count.pri,
              o->count.hup, o->count.unknown, o->count.total);

          struct perf_event_attr * attr = &c->events[e_idx].attr;
          fprintf(f_summary, "%d,%llu,%llu,%llu,%llu,%d,%d,%d,%llu,%llu",
              attr->type, attr->config,
              attr->sample_period,
              attr->sample_type,attr->read_format,
              attr->inherit, attr->pinned, attr->precise_ip,
              attr->config1, attr->config2);

          fprintf(f_summary,",%u", o->nr_overflows);
	  fprintf(f_summary,",%llu", c->events[e_idx].count_data[3+2*e_idx]);
	  fprintf(f_summary,",%llu", c->events[e_idx].count_data[1]);
  	  fprintf(f_summary,",%llu", c->events[e_idx].count_data[2]);
          fprintf(f_summary,"\n");
          if (o->count.total == 0)
            {
              printf("No event signals were received.\n\n");
            }
          else
            {
              printf("%d signals were received\n\n", o->count.total);
            }
          tot_signs += c->events[e_idx].output.count.total;
          tot_overflows += o->nr_overflows;
        }
    }
  printf("Total signals: %ld, Total overflows: %u\n", tot_signs, tot_overflows);
  fclose(f_summary);
  return PE_OK;
}

#endif /* PE_OUTPUT_H_ */
