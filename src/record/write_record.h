#ifndef _WRITE_RECORD_H
#define _WRITE_RECORD_H

#define _GNU_SOURCE 1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>

#include <errno.h>

#include <signal.h>

#include <sys/mman.h>

#include <sys/ioctl.h>
#include <asm/unistd.h>
#include <sys/prctl.h>
#include <linux/perf_event.h>

#include "mb.h"
#include "spe_sampling.h"

int
perf_mmap_read(pe_event_info_t * ev);

int
perf_mmap_read_all(pe_groups_t * g)
{
  int i, j;
  int nr_failures = 0;
  int written_bytes = 0;
  for (i = 0; i < g->num_groups; ++i)
    {

      pe_control_t *ctl = &g->groups[i];

      for (j = 0; j < ctl->num_events; ++j)
        {
          int w_bytes = perf_mmap_read(&ctl->events[j]);
          nr_failures += (w_bytes < 0);
          written_bytes += (w_bytes > 0) ? w_bytes : 0;
        }
    }
  return (nr_failures > 0) ? -nr_failures : written_bytes;
}
#endif
