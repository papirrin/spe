#ifndef SPE_RECORD
#define SPE_RECORD

#ifndef _GNU_SOURCE
  #define _GNU_SOURCE
#endif

#include <sched.h>
#include "spe_period.h"
#include "event_conf_types.h"
#include "spe_internal.h"

struct spe_record__conf{
  char output_preffix[FILENAME_MAX];
  char output_path[FILENAME_MAX];
  short int verbose;
  struct period_params period[PERF_EVENT_MAX_EVENTS];
  short int nr_events;
  char * const *wl_args;
  e_events_conf evs_conf;
  cpu_set_t record_aff_mask;
  cpu_set_t load_aff_mask;
};

struct workload_state{
  int cork_fd;
  pid_t pid;
  short int wait_child;
} wl_state;

int spe_record__record(struct spe_record__conf *conf);

#endif
