#ifndef _SPE_SAMPLING_H
#define _SPE_SAMPLING_H

#define _GNU_SOURCE 1

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include <string.h>
#include <errno.h>
#include <inttypes.h>
#include <fcntl.h>
//#include <sys/utsname.h>
//#include <sys/types.h>
#include <signal.h>

#include <sys/mman.h>
#include <sys/ioctl.h>
#include <asm/unistd.h>

#include "spe_internal.h"
#include "mb.h"
#include "spe_record.h"

#define OUTPUT_BUFF_SIZE 2<<16

typedef struct signal_counts
{
  int in, out, msg, err, pri, hup, unknown, total;
} pe_count_t;

typedef struct
{
  pe_count_t count;
  char output_buf[OUTPUT_BUFF_SIZE];
  FILE* output_fp;
  unsigned int nr_overflows;
} pe_output_t;

typedef struct
{
  int group_leader_fd; /* fd of group leader                   */
  int event_fd; /* fd of event                          */
  int event_opened; /* event successfully opened            */
  uint32_t nr_mmap_pages; /* number pages in the mmap buffer      */
  void *mmap_buf; /* used for control/profiling           */
  struct perf_event_attr attr; /* perf_event config structure          */
  pe_output_t output;
  long long count_data[3 + 3*PERF_EVENT_MAX_MPX_COUNTERS];
} pe_event_info_t;

typedef struct
{
  int num_events; /* number of events in control state */
  int cpu; /* which cpu to measure              */
  pid_t tid; /* thread we are monitoring          */
  pe_event_info_t events[PERF_EVENT_MAX_MPX_COUNTERS];
} pe_control_t;

typedef struct
{
  int num_groups;
  pe_control_t groups[PERF_EVENT_MAX_GROUPS];
} pe_groups_t;

pe_event_info_t *
find_event_from_fd(pe_groups_t * groups, int fd)
{
  int i, j;
  for (i = 0; i < groups->num_groups; ++i)
    {

      pe_control_t * ctl = &groups->groups[i];
      for (j = 0; j < ctl->num_events; ++j)
        if (ctl->events[j].event_fd == fd)
          {
            return &ctl->events[j];
          }
    }
  return NULL ;
}

void
sc_init(struct spe_record__conf * conf, void
(*pe_handler)(int, siginfo_t *, void *));

/* Set the F_SETOWN_EX flag on the fd.                          */
/* This affects which thread an overflow signal gets sent to    */
/* Handled in a subroutine to handle the fact that the behavior */
/* is dependent on kernel version.                              */
int
fcntl_setown_fd(int fd)
{

  int ret;
  struct f_owner_ex fown_ex;

  /* set ownership of the descriptor */
  fown_ex.type = F_OWNER_PID;
  fown_ex.pid = getpid();
  ret = fcntl(fd, F_SETOWN_EX, (unsigned long) &fown_ex);

  if (ret == -1)
    {
      REPORTERROR("cannot fcntl(F_SETOWN_EX) on %d: %s", fd, strerror(errno));
      return PE_ESYS;
    }
  return PE_OK;
}

int set_signal(int s, int fd){
	int ret = fcntl(fd, F_SETSIG, s);
	  if (ret == -1)
	      {
	        REPORTERROR("cannot fcntl(F_SETSIG,%d) on %d: %s", s, fd,
	            strerror(errno));
	        return PE_ESYS;
	      }
	  return PE_OK;
}
int
prepare_fd(pe_control_t *ctl, int evt_idx)
{
  int ret;
  void *buf_addr;
  int fd = ctl->events[evt_idx].event_fd;

  /* Register that we would like a SIGIO notification when a mmap'd page */
  /* becomes full.                                                       */
  ret = fcntl(fd, F_SETFL, O_ASYNC | O_RDWR | O_NONBLOCK);
  if (ret)
    {
      REPORTERROR("fcntl(%d, F_SETFL, O_ASYNC | O_NONBLOCK) "
          "returned error: %s", fd, strerror(errno));
      return PE_ESYS;
    }

  /* Set the F_SETOWN_EX flag on the fd.                          */
  /* This affects which thread an overflow signal gets sent to.   */
  ret = fcntl_setown_fd(fd);
  if (ret != PE_OK)
    return ret;

  /* Set FD_CLOEXEC.  Otherwise if we do an exec with an overflow */
  /* running, the overflow handler will continue into the exec()'d*/
  /* process and kill it because no signal handler is set up.     */

  /*TODO: Is this causing the problem to use inherit??*/
  ret = fcntl(fd, F_SETFD, FD_CLOEXEC);
  if (ret)
    {
      return PE_ESYS;
    }

  /* when you explicitely declare that you want a particular signal,  */
  /* eve  n with you use the default signal, the kernel will send more  */
  /* information concerning the event to the signal handler.          */
  /*                                                                  */
  /* In particular, it will send the file descriptor from which the   */
  /* event is originating which can be quite useful when monitoring   */
  /* multiple tasks from a single thread.                             */
  check_notneg(set_signal(POLL_IN, fd),"Error setting signal.\n");
  check_notneg(set_signal(POLL_OUT, fd),"Error setting signal.\n");
  check_notneg(set_signal(POLL_MSG, fd),"Error setting signal.\n");
  check_notneg(set_signal(POLL_ERR, fd),"Error setting signal.\n");
  check_notneg(set_signal(POLL_PRI, fd),"Error setting signal.\n");
  check_notneg(set_signal(POLL_HUP, fd),"Error setting signal.\n");
  check_notneg(set_signal(PE_SAMPLE_SIG,fd),"Error setting signal.\n");
  
  if (ret == -1)
    {
      REPORTERROR("cannot fcntl(F_SETSIG,%d) on %d: %s\n", PE_SAMPLE_SIG, fd,
          strerror(errno));
      return PE_ESYS;
    }

  /* mmap() the sample buffer */
  buf_addr = mmap(NULL, ctl->events[evt_idx].nr_mmap_pages * getpagesize(),
      PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  if (buf_addr == MAP_FAILED )
    {
      REPORTERROR("mmap(NULL,%d,%d,%d,%d,0): %s\n",
          ctl->events[evt_idx].nr_mmap_pages * getpagesize(), PROT_READ,
          MAP_SHARED, fd, strerror(errno));
      return (PE_ESYS);
    }

 //SUBDBG("Sample buffer for fd %d is located at %p\n", fd, buf_addr);

  /* Set up the mmap buffer and its associated helpers */
  ctl->events[evt_idx].mmap_buf = (struct perf_counter_mmap_page *) buf_addr;

  return PE_OK;
}

/* Open all events in the control state */
int
open_pe_events(pe_control_t *ctl);

/* Close all of the opened events */
int
close_pe_events(pe_control_t *ctl)
{
  int i;
  int num_closed = 0;
  int events_not_opened = 0;

  /* Close child events first */
  for (i = 0; i < ctl->num_events; i++)
    {

      if (ctl->events[i].event_opened)
        {

          if (ctl->events[i].group_leader_fd != -1)
            {
              if (ctl->events[i].mmap_buf)
                {
                  if (munmap(ctl->events[i].mmap_buf,
                      ctl->events[i].nr_mmap_pages * getpagesize()))
                    {
                      REPORTERROR("munmap of fd = %d returned error: %s",
                          ctl->events[i].event_fd, strerror(errno));
                      return PE_ESYS;
                    }
                }
              if (close(ctl->events[i].event_fd))
                {
                  REPORTERROR("close of fd = %d returned error: %s",
                      ctl->events[i].event_fd, strerror(errno));
                  return PE_ESYS;
                }
              else
                {
                  num_closed++;
                }
              ctl->events[i].event_opened = 0;
            }
        }
      else
        {
          events_not_opened++;
        }
    }

  /* Close the group leaders last */
  for (i = 0; i < ctl->num_events; i++)
    {

      if (ctl->events[i].event_opened)
        {

          if (ctl->events[i].group_leader_fd == -1)
            {
              if (ctl->events[i].mmap_buf)
                {
                  if (munmap(ctl->events[i].mmap_buf,
                      ctl->events[i].nr_mmap_pages * getpagesize()))
                    {
                      REPORTERROR("munmap of fd = %d returned error: %s",
                          ctl->events[i].event_fd, strerror(errno));
                      return PE_ESYS;
                    }
                }

              if (close(ctl->events[i].event_fd))
                {
                  REPORTERROR("close of fd = %d returned error: %s",
                      ctl->events[i].event_fd, strerror(errno));
                  return PE_ESYS;
                }
              else
                {
                  num_closed++;
                }
              ctl->events[i].event_opened = 0;
            }
        }
    }

  if (ctl->num_events != num_closed)
    {
      if (ctl->num_events != (num_closed + events_not_opened))
        {
          REPORTERROR("Didn't close all events: "
              "Closed %d Not Opened: %d Expected %d\n", num_closed,
              events_not_opened, ctl->num_events);
          return PE_EBUG;
        }
    }

  ctl->num_events = 0;

  return PE_OK;
}

inline static uint64_t
mmap_read_head(pe_event_info_t *pe)
{
  struct perf_event_mmap_page *pc = pe->mmap_buf;
  int head;

  if (pc == NULL )
    {
      REPORTERROR("perf_event_mmap_page is NULL");
      return 0;
    }

  head = pc->data_head;
  rmb( );

  return head;
}

inline static void
mmap_write_tail(pe_event_info_t *pe, uint64_t tail)
{
  struct perf_event_mmap_page *pc = pe->mmap_buf;
  //TODO: Memory barrier when writting tail does not seem to have effect
  //wmb( );
  /* ensure all reads are done before we write the tail out. */
  pc->data_tail = tail;
}

int
open_pe_group(pe_groups_t * g)
{
  int i, ret;

  for (i = 0; i < g->num_groups; ++i)
    {
      ret = open_pe_events(&g->groups[i]);
      if (ret != PE_OK)
        {
          return ret;
        }
    }
  return PE_OK;
}
int
close_pe_group(pe_groups_t * g)
{
  int i, ret;

  for (i = 0; i < g->num_groups; ++i)
    {
      ret = close_pe_events(&g->groups[i]);
      if (ret != PE_OK)
        {
          return ret;
        }
    }
  return PE_OK;
}

int
start_pe_group(pe_groups_t * g)
{
  int i, j, ret, fd;

  for (i = 0; i < g->num_groups; ++i)
    {
      for (j = 0; j < g->groups[i].num_events; ++j)
        {
          fd = g->groups[i].events[j].event_fd;
          ret = ioctl(fd, PERF_EVENT_IOC_RESET, 0);
          if (ret == -1)
            {
              REPORTERROR("Error starting group\n");
              return ret;
            }
          ret = ioctl(fd, PERF_EVENT_IOC_ENABLE, 0);

          if (ret == -1)
            {
              REPORTERROR("Error starting group\n");
              return ret;
            }
        }

    }
  return PE_OK;
}

int
stop_pe_group(pe_groups_t * g)
{
  int i, j, ret, fd;

  for (i = 0; i < g->num_groups; ++i)
    {
      for (j = 0; j < g->groups[i].num_events; ++j)
        {
          fd = g->groups[i].events[j].event_fd;
          ret = ioctl(fd, PERF_EVENT_IOC_DISABLE, 0);

          if (ret == -1)
            {
              REPORTERROR("Error stoping group\n");
              return ret;
            }
          ret = read(fd, &g->groups[i].events[j].count_data, (3+PERF_EVENT_MAX_MPX_COUNTERS)*sizeof(long long));
          if(ret < sizeof(long long)*4){
                REPORTERROR("Error reading final count. ErrorNr: %d, ErrorMsg: %s .\n", ret, strerror(errno));
                return ret;
          }
        }
    }
  return PE_OK;
}
#endif
