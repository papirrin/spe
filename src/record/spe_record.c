#define _GNU_SOURCE 1

#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#include "spe_internal.h"
#include "spe_period.c"
#include "event_conf.c"
#include "spe_output.h"
#include "spe_sampling.c"

void
print_spe_record__conf(const struct spe_record__conf * conf)
{
  int i = 0;
  char * const * wl_args = conf->wl_args;
  while (wl_args[i] != NULL )
    {
      printf("wl_args[%d]=%s\n", i, wl_args[i]);
      i++;
    }
  printf("output_preffix:\t%s\n", conf->output_preffix);

  for( i=0; i < conf->nr_events; ++i){
    if (!is_variable_period(&conf->period[i]))
      printf("fixed_period:\t%lu\n", conf->period[i].fixed_period);
    else
      printf("random period:\tseed:%d, uniform[%lu,%lu]\n", conf->period[i].seed,
          conf->period[i].min_period, conf->period[i].max_period);
  }

  printf("Verbose:\t%d\nEvents Configuration Type:\t%d\n", conf->verbose,
      conf->evs_conf);

}

int
spe__prepare_workload(char * const *argv)
{
  struct workload_state *wl = &wl_state;
  wl->wait_child = 1;
  int child_ready_pipe[2], go_pipe[2];
  char bf;

  if (pipe(child_ready_pipe) < 0)
    {
      perror("failed to create 'ready' pipe");
      return -1;
    }

  if (pipe(go_pipe) < 0)
    {
      perror("failed to create 'go' pipe");
      goto out_close_ready_pipe;
    }

  wl->pid = fork();
  if (wl->pid < 0)
    {
      perror("failed to fork");
      goto out_close_pipes;
    }

  if (!wl->pid)
    {
      /*TODO: What is this pipe for?*/
      /*if (opts->pipe_output)
       dup2(2, 1);
       */
      close(child_ready_pipe[0]);
      close(go_pipe[1]);
      fcntl(go_pipe[0], F_SETFD, FD_CLOEXEC);

      /*
       * Do a dummy execvp to get the PLT entry resolved,
       * so we avoid the resolver overhead on the real
       * execvp call.
       */
      execvp("", (char **) argv);

      /*
       * Tell the parent we're ready to go
       */
      close(child_ready_pipe[1]);

      /*
       * Wait until the parent tells us to go.
       */
      if (read(go_pipe[0], &bf, 1) == -1)
        perror("unable to read pipe");

      //printf("child opening:%s\n", argv[0]);
      if (execvp(argv[0], (char **) argv) == -1)
        {
          fprintf(stderr, "Child: Error en execvp %s", strerror(errno));
        }

      perror(argv[0]);
      kill(getppid(), SIGUSR1);
      exit(-1);
    }
  /*
   if (perf_target__none(&opts->target))
   evlist->threads->map[0] = evlist->workload.pid;
   */
  close(child_ready_pipe[1]);
  close(go_pipe[0]);
  /*
   * wait for child to settle
   */
  if (read(child_ready_pipe[0], &bf, 1) == -1)
    {
      perror("unable to read pipe\n");
      goto out_close_pipes;
    }

  wl->cork_fd = go_pipe[1];
  close(child_ready_pipe[0]);
  return 0;
  out_close_pipes: close(go_pipe[0]);
  close(go_pipe[1]);
  out_close_ready_pipe: close(child_ready_pipe[0]);
  close(child_ready_pipe[1]);
  return -1;
}

int
spe__start_workload()
{
  struct workload_state *wl = &wl_state;
  if (wl->cork_fd > 0)
    {
      /*
       * Remove the cork, let it rip!
       */
      return close(wl->cork_fd);
    }

  return 0;
}

static void
spe_record__handler(int signum)
{
  if (signum == SIGCHLD)
    {
      wl_state.wait_child = 0;
      printf("Child finished signal.\n");
    }
}

static pe_groups_t g_groups;

/* Recall that:
 * "By contrast, if multiple instances of a standard signal are delivered while that signal is currently blocked, then only one instance is queued."
 * So the signal count is not necessarily the same as the number of records.
 */
static void
pe_handler(int signum, siginfo_t *info, void * context)
{
  int fd = info->si_fd;
  pe_event_info_t * ev = find_event_from_fd(&g_groups, fd);
  //ioctl(fd, PERF_EVENT_IOC_REFRESH);
  //ioctl(fd, PERF_EVENT_IOC_DISABLE);
  check_notnull(ev,"Signal handler could not find event.\n");
  add_count(&ev->output, info->si_code);
  
}

int
spe_record__record(struct spe_record__conf *conf)
{

  signal(SIGCHLD, spe_record__handler);

  print_spe_record__conf(conf);

  spe__prepare_workload(conf->wl_args);

  /*
   * Set affinity masks
   */
  check_return(sched_setaffinity(0, sizeof(cpu_set_t), &conf->record_aff_mask),
      "Failed to set record process affinity mask, Error: %d\n", errno);
  check_return(sched_setaffinity(wl_state.pid, sizeof(cpu_set_t), &conf->load_aff_mask),
      "Failed to set load process affinity mask, Error: %d\n", errno);

  sc_init(conf, pe_handler);
  check_return(initialize_groups(&g_groups, conf, wl_state.pid),
      "Error initializing groups.\n");

  check_return(open_outputs(&g_groups, conf->output_path, conf->output_preffix),
      "Error opening outputs.\n");

  check_return(open_pe_group(&g_groups), "Error opening groups.\n");

  printf("Load and monitor processes are ready to go.\n");
  check_return(start_pe_group(&g_groups), "Error starting groups.\n");
  
  //TODO: The new bit enable_on_exec may be better than what we do now
  check_return(spe__start_workload(), "Error start_workload.\n");

  int awaken = 0, bytes_curr = 0;
  int bytes_written = 0;

  //TODO: How to improve efficiency of polling?
  int nr_signal_to_wait = 10;
  int i;

  while (wl_state.wait_child)
    {
      for (i = 0; wl_state.wait_child && i < nr_signal_to_wait; ++i)
        pause();
      ++awaken;
      bytes_curr = perf_mmap_read_all(&g_groups);
      if (bytes_curr < 0)
        {
          REPORTERROR("Error reading mmap.\n");
        }
      else
        {
          bytes_written += bytes_curr;
        }
    }

  perf_mmap_read_all(&g_groups);

  check_return(stop_pe_group(&g_groups), "Error stopping groups.\n");
  printf("End. Written %d bytes. Awaken %d times\n", bytes_written, awaken);

  //check_return(stop_pe_group(&g_groups), "Error stopping groups\n");

  check_return(print_summaries(&g_groups, conf->output_path, conf->output_preffix),
      "Error printing outputs.\n");

  check_return(close_outputs(&g_groups), "Error closing outputs.\n");
  check_return(close_pe_group(&g_groups), "Error closing groups\n");
  return 0;
}
