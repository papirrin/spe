/*
 * spe_random.h
 *
 *  Created on: Mar 31, 2013
 *      Author: papirrin
 */

#ifndef SPE_PERIOD_H_
#define SPE_PERIOD_H_

#define SIZE_RANDOM_STATE 256
struct period_params{
	long unsigned int fixed_period;
	/*Uniform Random*/
	int seed;
	long unsigned int min_period, max_period;

	/* Specific to random */
	char randstate[SIZE_RANDOM_STATE];
};

#endif /* SPE_RANDOM_H_ */
