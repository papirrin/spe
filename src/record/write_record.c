/* pare_record.c  */
/* by Vince Weaver   vincent.weaver _at_ maine.edu */

#include "write_record.h"
#include <assert.h>

int
perf_mmap_read(pe_event_info_t * ev)
{
  int size = 0;
  if (ev->nr_mmap_pages == 0)
    return PE_EBUG;

  if (ev->mmap_buf == NULL )
    return PE_EBUG;

  FILE * o_fp = ev->output.output_fp;

  struct perf_event_mmap_page * mpage =
      (struct perf_event_mmap_page *) ev->mmap_buf;

  void * data_start = ev->mmap_buf + getpagesize();
  unsigned int data_bitsize = (ev->nr_mmap_pages - 1) * getpagesize();

  long long data_head = mmap_read_head(ev);
  long long data_prev_head = mpage->data_tail;

  if (data_prev_head == data_head)
    {
      return 0;
    }

  // Overflow occurred
  if((data_head - data_prev_head )/data_bitsize > 0){
	  //printf("\n\nOverflooooooooooooowwwwwwww\n\n");
	  mmap_write_tail(ev, data_head);
	  ev->output.nr_overflows ++;
	  return 0;
  }
  //assert((data_head - data_prev_head )/data_bitsize <= 0);

  int diff_wrappings = data_head / data_bitsize - data_prev_head / data_bitsize;

  unsigned int data_head_off = data_head % data_bitsize;
  unsigned int data_prev_head_off = data_prev_head % data_bitsize;

  assert(diff_wrappings >= 0);
  assert(diff_wrappings <= 1);

  if (diff_wrappings)
    {

      assert(data_prev_head_off > data_head_off);
      size = data_bitsize - data_prev_head_off + data_head_off;
      fwrite(data_start + data_prev_head_off,
          data_bitsize - data_prev_head_off, 1, o_fp);
      fwrite(data_start, data_head_off, 1,o_fp);
    }
  else
    {
      assert(data_prev_head_off <= data_head_off);

      size = data_head_off - data_prev_head_off;

      fwrite(data_start + data_prev_head_off, size, 1, o_fp);
    }
  mmap_write_tail(ev, data_head);

  return size;

}
