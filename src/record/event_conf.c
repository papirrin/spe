/*
 * event_conf_types.c
 *
 *  Created on: Apr 5, 2013
 *      Author: papirrin
 */

#include <stdbool.h>
#include "event_conf.h"
#include "spe_output.h"
#include "spe_period.h"
#include <assert.h>

/**
 * nr_pages is restricted so that the mmap buffer is less than 1 MB, restricted due
 * to security reasons in the kernel and modify it require kernel recompilation
 * http://stackoverflow.com/questions/8213671/mmap-operation-not-permitted
 */
static const int nr_pages = 2<<7;

void spc_ctl_2_events(
	pe_groups_t *groups, const struct spe_record__conf * conf, 
	__u64 event1_type, __u64 event1_config, 
	__u64 event2_type, __u64 event2_config,
	 bool adaptive, bool multiplex) 
{
	const int read_format = PERF_FORMAT_GROUP |PERF_FORMAT_ID | PERF_FORMAT_TOTAL_TIME_ENABLED
                        | PERF_FORMAT_TOTAL_TIME_RUNNING;
	const int sample_type = PERF_SAMPLE_TIME
			| PERF_SAMPLE_READ
			| PERF_SAMPLE_ID
			| PERF_SAMPLE_PERIOD
			| PERF_SAMPLE_STREAM_ID;

	groups->num_groups = 1;
	pe_control_t * ctl = &groups->groups[0];
	ctl->cpu = -1;
	ctl->num_events = 2;

	ctl->events[0].group_leader_fd = -1;
	ctl->events[0].event_fd = -1;
	ctl->events[0].event_opened = 0;
	ctl->events[0].nr_mmap_pages = 1 + nr_pages;
	init_output(&ctl->events[0].output);

	struct perf_event_attr * attr = &(ctl->events[0].attr);
	memset(attr, 0, sizeof(struct perf_event_attr));

	attr->type = event1_type;
	attr->size = sizeof(struct perf_event_attr);
	attr->config = event1_config;

	attr->sample_period = get_next_period(&conf->period[0]);

	attr->disabled = 1;
	attr->pinned = 1;
	attr->exclude_kernel = 1;
	attr->exclude_hv = 1;
	attr->exclude_idle = 1;
        // Not sure if matters ...
        attr->exclude_callchain_kernel = 1;
        attr->exclude_callchain_user = 1;
//	attr->watermark = 1;
//	attr->wakeup_events = 100;

	attr->read_format = read_format;
	attr->sample_type = sample_type;
	//attr->inherit = 1;

	// Second event
	ctl->events[1].group_leader_fd = -1;
	ctl->events[1].event_fd = -1;
	ctl->events[1].event_opened = 0;
	ctl->events[1].nr_mmap_pages = 1 + nr_pages;
	init_output(&ctl->events[1].output);

	attr = &(ctl->events[1].attr);

	memset(attr, 0, sizeof(struct perf_event_attr));
	attr->type = event2_type;
	attr->size = sizeof(struct perf_event_attr);
	attr->config = event2_config;

	attr->sample_period = get_next_period(&conf->period[1]);;

	attr->disabled = 1;
	attr->pinned = 1;
	attr->exclude_kernel = 1;
	attr->exclude_hv = 1;
	attr->exclude_idle = 1;
	// Not sure if matters ...
	attr->exclude_callchain_kernel = 1;
	attr->exclude_callchain_user = 1;
//	attr->watermark = 1;
//	attr->wakeup_events = 100;

	attr->read_format = read_format;
	attr->sample_type = sample_type;
	attr->adaptive = (adaptive)?1:0;
	attr->multiplex = (multiplex)?1:0;
	return;
}

void spc_ctl_1_fixed(pe_groups_t *groups, const struct spe_record__conf * conf, 
		     __u64 event1_type, __u64 event1_config, 
		     bool adaptive, bool multiplex) {
        const int read_format = PERF_FORMAT_GROUP |PERF_FORMAT_ID | PERF_FORMAT_TOTAL_TIME_ENABLED
                        | PERF_FORMAT_TOTAL_TIME_RUNNING;
	const int sample_type = PERF_SAMPLE_TIME
			| PERF_SAMPLE_READ
			| PERF_SAMPLE_ID
			| PERF_SAMPLE_PERIOD
			| PERF_SAMPLE_STREAM_ID;

	groups->num_groups = 1;
	pe_control_t * ctl = &groups->groups[0];
	ctl->cpu = -1;
	ctl->num_events = 1;

	ctl->events[0].group_leader_fd = -1;
	ctl->events[0].event_fd = -1;
	ctl->events[0].event_opened = 0;
	ctl->events[0].nr_mmap_pages = 1 + nr_pages;
	init_output(&ctl->events[0].output);

	struct perf_event_attr * attr = &(ctl->events[0].attr);
	memset(attr, 0, sizeof(struct perf_event_attr));

	attr->type = event1_type;
	attr->size = sizeof(struct perf_event_attr);
	attr->config = event1_config;

	attr->sample_period = get_next_period(&conf->period[0]);

	attr->disabled = 1;
	attr->pinned = 1;
	attr->exclude_kernel = 1;
	attr->exclude_hv = 1;
	
	attr->watermark = 1;
	attr->wakeup_events = 100;

	attr->read_format = read_format;
	attr->sample_type = sample_type;
	attr->adaptive = (adaptive)?1:0;
	attr->multiplex = (multiplex)?1:0;

}

void spc_ctl_2_fixed(pe_groups_t *groups, const struct spe_record__conf * conf, 
		     __u64 event1_type, __u64 event1_config, 
		     __u64 event2_type, __u64 event2_config,
		     bool adaptive, bool multiplex) {
        const int read_format = PERF_FORMAT_GROUP |PERF_FORMAT_ID | PERF_FORMAT_TOTAL_TIME_ENABLED
                        | PERF_FORMAT_TOTAL_TIME_RUNNING;
	const int sample_type = PERF_SAMPLE_TIME
			| PERF_SAMPLE_READ
			| PERF_SAMPLE_ID
			| PERF_SAMPLE_PERIOD
			| PERF_SAMPLE_STREAM_ID;

	groups->num_groups = 1;
	pe_control_t * ctl = &groups->groups[0];
	ctl->cpu = -1;
	ctl->num_events = 2;

	ctl->events[0].group_leader_fd = -1;
	ctl->events[0].event_fd = -1;
	ctl->events[0].event_opened = 0;
	ctl->events[0].nr_mmap_pages = 1 + nr_pages;
	init_output(&ctl->events[0].output);

	struct perf_event_attr * attr = &(ctl->events[0].attr);
	memset(attr, 0, sizeof(struct perf_event_attr));

	attr->type = event1_type;
	attr->size = sizeof(struct perf_event_attr);
	attr->config = event1_config;

	attr->sample_period = get_next_period(&conf->period[0]);

	attr->disabled = 1;
	attr->pinned = 1;
	attr->exclude_kernel = 1;
	attr->exclude_hv = 1;
	
	attr->watermark = 1;
	attr->wakeup_events = 100;

	attr->read_format = read_format;
	attr->sample_type = sample_type;
	//attr->inherit = 1;

	// Second event
	ctl->events[1].group_leader_fd = -1;
	ctl->events[1].event_fd = -1;
	ctl->events[1].event_opened = 0;
	ctl->events[1].nr_mmap_pages = 1 + nr_pages;
	init_output(&ctl->events[1].output);

	attr = &(ctl->events[1].attr);

	memset(attr, 0, sizeof(struct perf_event_attr));
	attr->type = event2_type;
	attr->size = sizeof(struct perf_event_attr);
	attr->config = event2_config;

	attr->sample_period = get_next_period(&conf->period[1]);;

	attr->disabled = 1;
	attr->pinned = 1;
	attr->exclude_kernel = 1;
	attr->exclude_hv = 1;
	attr->watermark = 1;
	attr->wakeup_events = 100;

	attr->read_format = read_format;
	attr->sample_type = sample_type;
	attr->adaptive = (adaptive)?1:0;
	attr->multiplex = (multiplex)?1:0;

}

void spc_ctl_4_fixed(pe_groups_t *groups, const struct spe_record__conf * conf, bool adaptive, bool multiplex) {
        const int read_format = PERF_FORMAT_GROUP |PERF_FORMAT_ID | PERF_FORMAT_TOTAL_TIME_ENABLED
                        | PERF_FORMAT_TOTAL_TIME_RUNNING;
	const int sample_type = PERF_SAMPLE_TIME
			| PERF_SAMPLE_READ
			| PERF_SAMPLE_ID
			| PERF_SAMPLE_PERIOD
			| PERF_SAMPLE_STREAM_ID;

	groups->num_groups = 1;
	pe_control_t * ctl = &groups->groups[0];
	ctl->cpu = -1;
	ctl->num_events = 4;

	ctl->events[0].group_leader_fd = -1;
	ctl->events[0].event_fd = -1;
	ctl->events[0].event_opened = 0;
	ctl->events[0].nr_mmap_pages = 1 + nr_pages;
	init_output(&ctl->events[0].output);

	struct perf_event_attr * attr = &(ctl->events[0].attr);
	memset(attr, 0, sizeof(struct perf_event_attr));

	attr->type = PERF_TYPE_HARDWARE;
	attr->size = sizeof(struct perf_event_attr);
	attr->config = PERF_COUNT_HW_INSTRUCTIONS;

	attr->sample_period = get_next_period(&conf->period[0]);

	attr->disabled = 1;
	attr->pinned = 1;
	attr->exclude_kernel = 1;
	attr->exclude_hv = 1;
	
	attr->watermark = 1;
	attr->wakeup_events = 100;

	attr->read_format = read_format;
	attr->sample_type = sample_type;
	attr->adaptive = (adaptive)?1:0;
	attr->multiplex = (multiplex)?1:0;
	//attr->inherit = 1;

	// Second event
	ctl->events[1].group_leader_fd = -1;
	ctl->events[1].event_fd = -1;
	ctl->events[1].event_opened = 0;
	ctl->events[1].nr_mmap_pages = 1 + nr_pages;
	init_output(&ctl->events[1].output);

	attr = &(ctl->events[1].attr);

	memset(attr, 0, sizeof(struct perf_event_attr));
	attr->type = PERF_TYPE_HARDWARE;
	attr->size = sizeof(struct perf_event_attr);
	attr->config = PERF_COUNT_HW_CACHE_REFERENCES;

	attr->sample_period = get_next_period(&conf->period[1]);;

	attr->disabled = 1;
	attr->pinned = 1;
	attr->exclude_kernel = 1;
	attr->exclude_hv = 1;
	attr->watermark = 1;
	attr->wakeup_events = 100;

	attr->read_format = read_format;
	attr->sample_type = sample_type;
	attr->adaptive = (adaptive)?1:0;
	attr->multiplex = (multiplex)?1:0;

        // Third event
        ctl->events[2].group_leader_fd = -1;
        ctl->events[2].event_fd = -1;
        ctl->events[2].event_opened = 0;
        ctl->events[2].nr_mmap_pages = 1 + nr_pages;
        init_output(&ctl->events[2].output);

        attr = &(ctl->events[2].attr);

        memset(attr, 0, sizeof(struct perf_event_attr));
        attr->type = PERF_TYPE_HARDWARE;
        attr->size = sizeof(struct perf_event_attr);
        attr->config = PERF_COUNT_HW_CACHE_MISSES;

        attr->sample_period = get_next_period(&conf->period[2]);

        attr->disabled = 1;
        attr->pinned = 1;
        attr->exclude_kernel = 1;
        attr->exclude_hv = 1;
        attr->watermark = 1;
        attr->wakeup_events = 100;

        attr->read_format = read_format;
        attr->sample_type = sample_type;
        attr->adaptive = (adaptive)?1:0;
	attr->multiplex = (multiplex)?1:0;

	// Fourth event
        ctl->events[3].group_leader_fd = -1;
        ctl->events[3].event_fd = -1;
        ctl->events[3].event_opened = 0;
        ctl->events[3].nr_mmap_pages = 1 + nr_pages;
        init_output(&ctl->events[3].output);

        attr = &(ctl->events[3].attr);

        memset(attr, 0, sizeof(struct perf_event_attr));
        attr->type = PERF_TYPE_HARDWARE;
        attr->size = sizeof(struct perf_event_attr);
        attr->config = PERF_COUNT_HW_BRANCH_MISSES;

        attr->sample_period = get_next_period(&conf->period[3]);;

        attr->disabled = 1;
        attr->pinned = 1;
        attr->exclude_kernel = 1;
        attr->exclude_hv = 1;
        attr->watermark = 1;
        attr->wakeup_events = 100;

        attr->read_format = read_format;
        attr->sample_type = sample_type;
        attr->adaptive = (adaptive)?1:0;
	attr->multiplex = (multiplex)?1:0;

}

void spc_ctl_8_mpx(pe_groups_t *groups, const struct spe_record__conf * conf, 
		   __u64 event1_type, __u64 event1_config, 
		   __u64 event2_type, __u64 event2_config,
		   __u64 event3_type, __u64 event3_config, 
		   __u64 event4_type, __u64 event4_config,
		   __u64 event5_type, __u64 event5_config, 
		   __u64 event6_type, __u64 event6_config,
		   __u64 event7_type, __u64 event7_config, 
		   __u64 event8_type, __u64 event8_config,
		   bool adaptive, bool multiplex)
{
	const int read_format = PERF_FORMAT_GROUP |PERF_FORMAT_ID | PERF_FORMAT_TOTAL_TIME_ENABLED
                        | PERF_FORMAT_TOTAL_TIME_RUNNING;
	const int sample_type = PERF_SAMPLE_TIME
			| PERF_SAMPLE_READ
			| PERF_SAMPLE_ID
			| PERF_SAMPLE_PERIOD
			| PERF_SAMPLE_STREAM_ID;
	struct perf_event_attr * attr1 = NULL;
	struct perf_event_attr * attr2 = NULL;
	struct perf_event_attr * attr3 = NULL;
	struct perf_event_attr * attr4 = NULL;
	struct perf_event_attr * attr5 = NULL;
	struct perf_event_attr * attr6 = NULL;
	struct perf_event_attr * attr7 = NULL;
	struct perf_event_attr * attr8 = NULL;

	groups->num_groups = 8;
	pe_control_t * ctl1 = &groups->groups[0];
	pe_control_t * ctl2 = &groups->groups[1];
	pe_control_t * ctl3 = &groups->groups[2];
	pe_control_t * ctl4 = &groups->groups[3];
	pe_control_t * ctl5 = &groups->groups[4];
	pe_control_t * ctl6 = &groups->groups[5];
	pe_control_t * ctl7 = &groups->groups[6];
	pe_control_t * ctl8 = &groups->groups[7];

	// Event1
	ctl1->cpu = -1;
	ctl1->num_events = 1;
	ctl1->events[0].group_leader_fd = -1;
	ctl1->events[0].event_fd = -1;
	ctl1->events[0].event_opened = 0;
	ctl1->events[0].nr_mmap_pages = 1 + nr_pages;
	init_output(&ctl1->events[0].output);
	attr1 = &(ctl1->events[0].attr);
	memset(attr1, 0, sizeof(struct perf_event_attr));
	attr1->type = event1_type;
	attr1->size = sizeof(struct perf_event_attr);
	attr1->config = event1_config;
	attr1->sample_period = get_next_period(&conf->period[0]);
	//attr1->disabled = 1;
	attr1->exclude_kernel = 1;
	attr1->exclude_hv = 1;
	attr1->exclude_idle = 1;
        // Not sure if matters ...
        attr1->exclude_callchain_kernel = 1;
        attr1->exclude_callchain_user = 1;
	attr1->read_format = read_format;
	attr1->sample_type = sample_type;
	attr1->adaptive = (adaptive)?1:0;
	attr1->multiplex = (multiplex)?1:0;

	// Event2
	ctl2->cpu = -1;
	ctl2->num_events = 1;
	ctl2->events[0].group_leader_fd = -1;
	ctl2->events[0].event_fd = -1;
	ctl2->events[0].event_opened = 0;
	ctl2->events[0].nr_mmap_pages = 1 + nr_pages;
	init_output(&ctl2->events[0].output);
	attr2 = &(ctl2->events[0].attr);
	memset(attr2, 0, sizeof(struct perf_event_attr));
	attr2->type = event2_type;
	attr2->size = sizeof(struct perf_event_attr);
	attr2->config = event2_config;
	attr2->sample_period = get_next_period(&conf->period[1]);;
	attr2->disabled = 1;
	attr2->exclude_kernel = 1;
	attr2->exclude_hv = 1;
	attr2->exclude_idle = 1;
	// Not sure if matters ...
	attr2->exclude_callchain_kernel = 1;
	attr2->exclude_callchain_user = 1;
	attr2->read_format = read_format;
	attr2->sample_type = sample_type;
	attr2->adaptive = (adaptive)?1:0;
	attr2->multiplex = (multiplex)?1:0;

	// Event3
	ctl3->cpu = -1;
	ctl3->num_events = 1;
	ctl3->events[0].group_leader_fd = -1;
	ctl3->events[0].event_fd = -1;
	ctl3->events[0].event_opened = 0;
	ctl3->events[0].nr_mmap_pages = 1 + nr_pages;
	init_output(&ctl3->events[0].output);
	attr3 = &(ctl3->events[0].attr);
	memset(attr3, 0, sizeof(struct perf_event_attr));
	attr3->type = event3_type;
	attr3->size = sizeof(struct perf_event_attr);
	attr3->config = event3_config;
	attr3->sample_period = get_next_period(&conf->period[2]);
	attr3->disabled = 1;
	attr3->exclude_kernel = 1;
	attr3->exclude_hv = 1;
	attr3->exclude_idle = 1;
        // Not sure if matters ...
        attr3->exclude_callchain_kernel = 1;
        attr3->exclude_callchain_user = 1;
	attr3->read_format = read_format;
	attr3->sample_type = sample_type;
	attr3->adaptive = (adaptive)?1:0;
	attr3->multiplex = (multiplex)?1:0;

	// Event4
	ctl4->cpu = -1;
	ctl4->num_events = 1;
	ctl4->events[0].group_leader_fd = -1;
	ctl4->events[0].event_fd = -1;
	ctl4->events[0].event_opened = 0;
	ctl4->events[0].nr_mmap_pages = 1 + nr_pages;
	init_output(&ctl4->events[0].output);
	attr4 = &(ctl4->events[0].attr);
	memset(attr4, 0, sizeof(struct perf_event_attr));
	attr4->type = event4_type;
	attr4->size = sizeof(struct perf_event_attr);
	attr4->config = event4_config;
	attr4->sample_period = get_next_period(&conf->period[3]);;
	attr4->disabled = 1;
	attr4->exclude_kernel = 1;
	attr4->exclude_hv = 1;
	attr4->exclude_idle = 1;
	// Not sure if matters ...
	attr4->exclude_callchain_kernel = 1;
	attr4->exclude_callchain_user = 1;
	attr4->read_format = read_format;
	attr4->sample_type = sample_type;
	attr4->adaptive = (adaptive)?1:0;
	attr4->multiplex = (multiplex)?1:0;

	// Event5
	ctl5->cpu = -1;
	ctl5->num_events = 1;
	ctl5->events[0].group_leader_fd = -1;
	ctl5->events[0].event_fd = -1;
	ctl5->events[0].event_opened = 0;
	ctl5->events[0].nr_mmap_pages = 1 + nr_pages;
	init_output(&ctl5->events[0].output);
	attr5 = &(ctl5->events[0].attr);
	memset(attr5, 0, sizeof(struct perf_event_attr));
	attr5->type = event5_type;
	attr5->size = sizeof(struct perf_event_attr);
	attr5->config = event5_config;
	attr5->sample_period = get_next_period(&conf->period[4]);
	attr5->disabled = 1;
	attr5->exclude_kernel = 1;
	attr5->exclude_hv = 1;
	attr5->exclude_idle = 1;
        // Not sure if matters ...
        attr5->exclude_callchain_kernel = 1;
        attr5->exclude_callchain_user = 1;
	attr5->read_format = read_format;
	attr5->sample_type = sample_type;
	attr5->adaptive = (adaptive)?1:0;
	attr5->multiplex = (multiplex)?1:0;

	// Event6
	ctl6->cpu = -1;
	ctl6->num_events = 1;
	ctl6->events[0].group_leader_fd = -1;
	ctl6->events[0].event_fd = -1;
	ctl6->events[0].event_opened = 0;
	ctl6->events[0].nr_mmap_pages = 1 + nr_pages;
	init_output(&ctl6->events[0].output);
	attr6 = &(ctl6->events[0].attr);
	memset(attr6, 0, sizeof(struct perf_event_attr));
	attr6->type = event6_type;
	attr6->size = sizeof(struct perf_event_attr);
	attr6->config = event6_config;
	attr6->sample_period = get_next_period(&conf->period[5]);
	attr6->disabled = 1;
	attr6->exclude_kernel = 1;
	attr6->exclude_hv = 1;
	attr6->exclude_idle = 1;
	// Not sure if matters ...
	attr6->exclude_callchain_kernel = 1;
	attr6->exclude_callchain_user = 1;
	attr6->read_format = read_format;
	attr6->sample_type = sample_type;
	attr6->adaptive = (adaptive)?1:0;
	attr6->multiplex = (multiplex)?1:0;

	// Event7
	ctl7->cpu = -1;
	ctl7->num_events = 1;
	ctl7->events[0].group_leader_fd = -1;
	ctl7->events[0].event_fd = -1;
	ctl7->events[0].event_opened = 0;
	ctl7->events[0].nr_mmap_pages = 1 + nr_pages;
	init_output(&ctl7->events[0].output);
	attr7 = &(ctl7->events[0].attr);
	memset(attr7, 0, sizeof(struct perf_event_attr));
	attr7->type = event7_type;
	attr7->size = sizeof(struct perf_event_attr);
	attr7->config = event7_config;
	attr7->sample_period = get_next_period(&conf->period[6]);
	attr7->disabled = 1;
	attr7->exclude_kernel = 1;
	attr7->exclude_hv = 1;
	attr7->exclude_idle = 1;
        // Not sure if matters ...
        attr7->exclude_callchain_kernel = 1;
        attr7->exclude_callchain_user = 1;
	attr7->read_format = read_format;
	attr7->sample_type = sample_type;
	attr7->adaptive = (adaptive)?1:0;
	attr7->multiplex = (multiplex)?1:0;

	// Event8
	ctl8->cpu = -1;
	ctl8->num_events = 1;
	ctl8->events[0].group_leader_fd = -1;
	ctl8->events[0].event_fd = -1;
	ctl8->events[0].event_opened = 0;
	ctl8->events[0].nr_mmap_pages = 1 + nr_pages;
	init_output(&ctl8->events[0].output);
	attr8 = &(ctl8->events[0].attr);
	memset(attr8, 0, sizeof(struct perf_event_attr));
	attr8->type = event8_type;
	attr8->size = sizeof(struct perf_event_attr);
	attr8->config = event8_config;
	attr8->sample_period = get_next_period(&conf->period[7]);
	attr8->disabled = 1;
	attr8->exclude_kernel = 1;
	attr8->exclude_hv = 1;
	attr8->exclude_idle = 1;
	// Not sure if matters ...
	attr8->exclude_callchain_kernel = 1;
	attr8->exclude_callchain_user = 1;
	attr8->read_format = read_format;
	attr8->sample_type = sample_type;
	attr8->adaptive = (adaptive)?1:0;
	attr8->multiplex = (multiplex)?1:0;

	return;
}

__u64 cache_conf(__u64 cache_id, __u64 cache_op, __u64 cache_op_result){
	return( (cache_id) | (cache_op << 8) | (cache_op_result << 16));
}
int initialize_groups(pe_groups_t * groups, const struct spe_record__conf * conf, int pid) {
        assert(groups);
        assert(conf);
        switch (conf->evs_conf) {
/***********************
* 	Adaptive
***********************/
	case SPC_CTL_ADAPTIVE0:
	  spc_ctl_2_events(groups,conf,
		PERF_TYPE_HARDWARE,
		PERF_COUNT_HW_REF_CPU_CYCLES,
		PERF_TYPE_HARDWARE,
		PERF_COUNT_HW_INSTRUCTIONS, true, false);
		break;
        case SPC_CTL_ADAPTIVE1:
	  spc_ctl_2_events(groups, conf, 
		PERF_TYPE_HARDWARE,
		PERF_COUNT_HW_INSTRUCTIONS,
		PERF_TYPE_HW_CACHE, 
		cache_conf(
			PERF_COUNT_HW_CACHE_L1D, 
			PERF_COUNT_HW_CACHE_OP_READ, 
			PERF_COUNT_HW_CACHE_RESULT_MISS), true, false);
		break;
        case SPC_CTL_ADAPTIVE2:
          spc_ctl_2_events(groups, conf,
                PERF_TYPE_HARDWARE,
                PERF_COUNT_HW_INSTRUCTIONS,
                PERF_TYPE_HW_CACHE,
                cache_conf(
                        PERF_COUNT_HW_CACHE_L1D, 
                        PERF_COUNT_HW_CACHE_OP_WRITE, 
                        PERF_COUNT_HW_CACHE_RESULT_MISS), true, false);
                break;
        case SPC_CTL_ADAPTIVE3:
          spc_ctl_2_events(groups, conf,
                PERF_TYPE_HARDWARE,
                PERF_COUNT_HW_INSTRUCTIONS,
                PERF_TYPE_HW_CACHE,
                cache_conf(
                        PERF_COUNT_HW_CACHE_L1I, 
                        PERF_COUNT_HW_CACHE_OP_READ, 
                        PERF_COUNT_HW_CACHE_RESULT_ACCESS), true, false);
                break;
        case SPC_CTL_ADAPTIVE4:
          spc_ctl_2_events(groups, conf,
                PERF_TYPE_HARDWARE,
                PERF_COUNT_HW_INSTRUCTIONS,
                PERF_TYPE_HW_CACHE,
                cache_conf(
                        PERF_COUNT_HW_CACHE_L1I, 
                        PERF_COUNT_HW_CACHE_OP_READ, 
                        PERF_COUNT_HW_CACHE_RESULT_MISS), true, false);
                break;
/************************************
* Non Adaptive
************************************/
        case SPC_CTL_NON_ADAPTIVE0:
          spc_ctl_2_events(groups,conf,
                PERF_TYPE_HARDWARE,
                PERF_COUNT_HW_REF_CPU_CYCLES,
                PERF_TYPE_HARDWARE,
                PERF_COUNT_HW_INSTRUCTIONS, false, false);
                break;
        case SPC_CTL_NON_ADAPTIVE1:
          spc_ctl_2_events(groups, conf,
                PERF_TYPE_HARDWARE,
                PERF_COUNT_HW_INSTRUCTIONS,
                PERF_TYPE_HW_CACHE,
                cache_conf(
                        PERF_COUNT_HW_CACHE_L1D, 
                        PERF_COUNT_HW_CACHE_OP_READ, 
                        PERF_COUNT_HW_CACHE_RESULT_MISS), false, false);
//			PERF_COUNT_HW_CACHE_RESULT_MISS), true, false);
                break;
        case SPC_CTL_NON_ADAPTIVE2:
          spc_ctl_2_events(groups, conf,
                PERF_TYPE_HARDWARE,
                PERF_COUNT_HW_INSTRUCTIONS,
                PERF_TYPE_HW_CACHE,
                cache_conf(
                        PERF_COUNT_HW_CACHE_L1D,
                        PERF_COUNT_HW_CACHE_OP_WRITE, 
                        PERF_COUNT_HW_CACHE_RESULT_MISS), false, false);
                break;
        case SPC_CTL_NON_ADAPTIVE3:
          spc_ctl_2_events(groups, conf,
                PERF_TYPE_HARDWARE,
                PERF_COUNT_HW_INSTRUCTIONS,
                PERF_TYPE_HW_CACHE,
                cache_conf(
                        PERF_COUNT_HW_CACHE_L1I,
                        PERF_COUNT_HW_CACHE_OP_READ,
                        PERF_COUNT_HW_CACHE_RESULT_ACCESS), false, false);
                break;
        case SPC_CTL_NON_ADAPTIVE4:
          spc_ctl_2_events(groups, conf,
                PERF_TYPE_HARDWARE,
                PERF_COUNT_HW_INSTRUCTIONS,
                PERF_TYPE_HW_CACHE,
                cache_conf(
                        PERF_COUNT_HW_CACHE_L1I,
                        PERF_COUNT_HW_CACHE_OP_READ,
                        PERF_COUNT_HW_CACHE_RESULT_MISS), false, false);
                break;

/***********************
* 	Multiplex
***********************/
        case SPC_CTL_MULTIPLEX0:
	  spc_ctl_1_fixed(groups, conf, 
			  PERF_TYPE_HARDWARE, PERF_COUNT_HW_INSTRUCTIONS,
			  false, true);
		break;
        case SPC_CTL_MULTIPLEX1:
	  spc_ctl_1_fixed(groups, conf, 
			  PERF_TYPE_HARDWARE, PERF_COUNT_HW_REF_CPU_CYCLES,
			  false, true);
		break;
        case SPC_CTL_MULTIPLEX2:
	  spc_ctl_1_fixed(groups, conf, 
			  PERF_TYPE_HARDWARE, PERF_COUNT_HW_BRANCH_MISSES,
			  false, true);
		break;
        case SPC_CTL_MULTIPLEX3:
	  spc_ctl_1_fixed(groups, conf, 
			  PERF_TYPE_HW_CACHE, 
			  (PERF_COUNT_HW_CACHE_L1D |
			   ((PERF_COUNT_HW_CACHE_OP_READ) << 8) |
			   ((PERF_COUNT_HW_CACHE_RESULT_MISS) << 16)), 
			  false, true);
		break;
        case SPC_CTL_MULTIPLEX4:
	  spc_ctl_1_fixed(groups, conf, 
			  PERF_TYPE_HW_CACHE, 
			  (PERF_COUNT_HW_CACHE_L1I |
			   ((PERF_COUNT_HW_CACHE_OP_READ) << 8) |
			   ((PERF_COUNT_HW_CACHE_RESULT_MISS) << 16)), 
			  false, true);
		break;
        case SPC_CTL_MULTIPLEX5:
	  spc_ctl_1_fixed(groups, conf, 
			  PERF_TYPE_HW_CACHE,
			  (PERF_COUNT_HW_CACHE_LL |
			   ((PERF_COUNT_HW_CACHE_OP_READ) << 8) |
			   ((PERF_COUNT_HW_CACHE_RESULT_MISS) << 16)), 
			  false, true);
		break;
        case SPC_CTL_MULTIPLEX6:
	  spc_ctl_1_fixed(groups, conf, 
			  PERF_TYPE_HW_CACHE,
			  (PERF_COUNT_HW_CACHE_DTLB |
			   ((PERF_COUNT_HW_CACHE_OP_READ) << 8) |
			   ((PERF_COUNT_HW_CACHE_RESULT_MISS) << 16)), 
			  false, true);
		break;
        case SPC_CTL_MULTIPLEX7:
	  spc_ctl_1_fixed(groups, conf, 
			  PERF_TYPE_HW_CACHE, 
			  (PERF_COUNT_HW_CACHE_ITLB |
			   ((PERF_COUNT_HW_CACHE_OP_READ) << 8) |
			   ((PERF_COUNT_HW_CACHE_RESULT_MISS) << 16)), 
			  false, true);
		break;
        case SPC_CTL_MULTIPLEX8:
	  spc_ctl_8_mpx(groups, conf,
			PERF_TYPE_HARDWARE, PERF_COUNT_HW_INSTRUCTIONS,
			PERF_TYPE_HARDWARE, PERF_COUNT_HW_REF_CPU_CYCLES,
			PERF_TYPE_HARDWARE, PERF_COUNT_HW_BRANCH_MISSES,
			PERF_TYPE_HW_CACHE, 
			(PERF_COUNT_HW_CACHE_L1D |
			 ((PERF_COUNT_HW_CACHE_OP_READ) << 8) |
			 ((PERF_COUNT_HW_CACHE_RESULT_MISS) << 16)), 
			PERF_TYPE_HW_CACHE, 
			(PERF_COUNT_HW_CACHE_L1I |
			 ((PERF_COUNT_HW_CACHE_OP_READ) << 8) |
			 ((PERF_COUNT_HW_CACHE_RESULT_MISS) << 16)), 
			PERF_TYPE_HW_CACHE, 
			(PERF_COUNT_HW_CACHE_LL |
			 ((PERF_COUNT_HW_CACHE_OP_READ) << 8) |
			 ((PERF_COUNT_HW_CACHE_RESULT_MISS) << 16)), 
			PERF_TYPE_HW_CACHE, 			
			(PERF_COUNT_HW_CACHE_DTLB |
			 ((PERF_COUNT_HW_CACHE_OP_READ) << 8) |
			 ((PERF_COUNT_HW_CACHE_RESULT_MISS) << 16)), 
			PERF_TYPE_HW_CACHE, 
			(PERF_COUNT_HW_CACHE_ITLB |
			 ((PERF_COUNT_HW_CACHE_OP_READ) << 8) |
			 ((PERF_COUNT_HW_CACHE_RESULT_MISS) << 16)), 
			false, true);
                break;
        case SPC_CTL_MULTIPLEX9:
	  // IPC
          spc_ctl_2_fixed(groups, conf,
			  PERF_TYPE_HARDWARE, PERF_COUNT_HW_INSTRUCTIONS,
			  PERF_TYPE_HARDWARE, PERF_COUNT_HW_REF_CPU_CYCLES,  
			  false, true);
                break;

        default:
                REPORTERROR("Invalid events_conf value %d\n", conf->evs_conf);
                return PE_EBUG;
                break;
        }
        int i;
        for(i=0; i < groups->num_groups; ++i){
                groups->groups[i].tid = pid;
        }

        return PE_OK;
}
