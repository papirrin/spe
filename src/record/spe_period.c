#include "spe_period.h"

long int
is_variable_period(const struct period_params * pp)
{
  if (pp->fixed_period == 0)
    {
      return 1;
    }
  return 0;
}

long int
get_next_period(const struct period_params * pp)
{
  if (is_variable_period(pp))
    {
      if (pp->max_period == pp->min_period)
        {
          printf("Range of uniform variable is zero, random period: %ld\n", pp->min_period);
          return pp->min_period;
        }
      long int p = pp->min_period
          + random() % (pp->max_period - pp->min_period + 1);
      printf("New random period: %ld\n", p);
      return p;
    }
  return pp->fixed_period;
}

