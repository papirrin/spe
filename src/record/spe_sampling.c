/*
 * pe_sampling.c
 *
 *  Created on: Mar 24, 2013
 *      Author: David Carrillo-Cisneros
 */

#include "spe_sampling.h"
#include "write_record.c"
#include "spe_output.h"
#include "spe_period.h"

#include <assert.h>

/* Open all events in the control state */
int
open_pe_events(pe_control_t *ctl)
{

  int i, ret = PE_OK;
  long pid = ctl->tid;

  for (i = 0; i < ctl->num_events; i++)
    {

      ctl->events[i].event_opened = 0;

      /* set up the attr structure.  We don't set up all fields here */
      /* as some have already been set up previously.                */
 
      /* group leader (event 0) is special                */
      // group with multiple events
      if ((i == 0) && (ctl->num_events > 1)) {
	ctl->events[i].attr.pinned = 1;
	ctl->events[i].attr.disabled = 1;
	ctl->events[i].group_leader_fd = -1;
      }
      else { 
	if(ctl->num_events == 1) // single event (mpx)
	  ctl->events[i].attr.disabled = 1;
	else {                   // part of some group
	  ctl->events[i].attr.pinned = 0;
	  ctl->events[i].attr.disabled = 0;
	}
	ctl->events[i].group_leader_fd = ctl->events[0].event_fd;
      }

      /* try to open */
      ctl->events[i].event_fd = sys_perf_event_open(&ctl->events[i].attr, pid,
          ctl->cpu, ctl->events[i].group_leader_fd, 0 /* flags */
          );

      if (ctl->events[i].event_fd == -1)
        {
          SUBDBG("sys_perf_event_open returned error on event #%d."
              "  Error: %s\n", i, strerror(errno));
          if (errno == EPERM)
            ret = PE_EPERM;
          else
            ret = PE_ECNFLCT;
          goto open_pe_cleanup;
        }

      /*SUBDBG("sys_perf_event_open: tid: %ld, cpu_num: %d,"
          " group_leader/fd: %d, event_fd: %d,"
          " read_format: 0x%"PRIu64"\n", pid, ctl->cpu,
          ctl->events[i].group_leader_fd, ctl->events[i].event_fd,
          ctl->events[i].attr.read_format);*/

      ctl->events[i].event_opened = 1;
    }

  /* Now that we've successfully opened all of the events, do whatever  */
  /* "tune-up" is needed to attach the mmap'd buffers, signal handlers, */
  /* and so on.                                                         */
  for (i = 0; i < ctl->num_events; i++)
    {

      /* If sampling is enabled, hook up signal handler */
      if (ctl->events[i].attr.sample_period)
        {
          ret = prepare_fd(ctl, i);
          if (ret != PE_OK)
            {
              /* All of the fds are open, so we need to clean up all of them */
              i = ctl->num_events;
              goto open_pe_cleanup;
            }
        }
      else
        {
          /* Make sure this is NULL so close_pe_events works right */
          ctl->events[i].mmap_buf = NULL;
        }
    }

  return PE_OK;

  open_pe_cleanup:
  /* We encountered an error, close up the fds we successfully opened.  */
  /* We go backward in an attempt to close group leaders last, although */
  /* That's probably not strictly necessary.                            */
  while (i > 0)
    {
      i--;
      if (ctl->events[i].event_fd >= 0)
        {
          close(ctl->events[i].event_fd);
          ctl->events[i].event_opened = 0;
        }
    }

  return ret;
}

void
sc_init(struct spe_record__conf * conf, void
(*pe_handler)(int, siginfo_t *, void *))
{

  //TODO: One random generator per period
  if (initstate(conf->period[0].seed, conf->period[0].randstate,
      SIZE_RANDOM_STATE) == NULL)
    {
      fprintf(stderr,
          "Error while initializing the random number generator.\n");
    }

  //Set signal stuff
  struct sigaction sa;
  memset(&sa, 0, sizeof(struct sigaction));
  sa.sa_sigaction = pe_handler;
  sa.sa_flags = SA_SIGINFO;

  if (sigaction(PE_SAMPLE_SIG, &sa, NULL ) < 0)
    {
      fprintf(stderr, "Error setting up signal handler\n");
      exit(1);
    }
}

