/*
 * spe_parse.c
 *
 *  Created on: Apr 5, 2013
 *      Author: papirrin
 */

#include <stdio.h>
#include <stdlib.h>
#include "spe_parse.h"
#include "spe_internal.h"
#include "prints.h"
#include "output_manager.h"
#include <string.h>
#include <assert.h>

#define SAMPLE_BUFF_SIZE 100000

struct parse_files
{
  int nr_files;
  struct parse_file
  {
    int g_idx, e_idx;
    char filename[FILENAME_MAX];
    long long unsigned config;
    long long unsigned event_type;
    long long unsigned sample_type;
    long long unsigned read_format;
  } files[PERF_EVENT_MAX_GROUPS * PERF_EVENT_MAX_MPX_COUNTERS];
};

/* TODO: Now it does not really processes the csv, it assumes fix column order */
int
read_summary(struct spe_parse__conf * conf, struct parse_files * pf)
{
  size_t len, read;
  char buff[FILENAME_MAX];

  char * line;
  FILE * f_summary = fopen(conf->input_summ_fn, "r");
  check_notnull(f_summary, "Failed to open summary file %s\n", buff);

  pf->nr_files = 0;
  int skip_line = 1;

  while ((read = getline(&line, &len, f_summary) != -1))
    {
      if (skip_line)
        {
          skip_line = 0;
          continue;
        }

      int new_g = atoi(strtok(line, ","));
      assert(new_g >= 0);
      pf->files[pf->nr_files].g_idx = new_g;

      int new_e = atoi(strtok(NULL, ","));
      assert(new_e >= 0);
      pf->files[pf->nr_files].e_idx = new_e;

      char * new_fn = strtok(NULL, ",");
      assert(new_fn != NULL);
      strcpy(pf->files[pf->nr_files].filename, new_fn);

      int to_next = 8;
      int i = 0;
      for (; i < to_next; ++i)
        {
          strtok(NULL, ",");
        }
      pf->files[pf->nr_files].event_type = atol(strtok(NULL, ","));
      pf->files[pf->nr_files].config = atol(strtok(NULL, ","));
      // Skip init_period
      strtok(NULL, ",");
      pf->files[pf->nr_files].sample_type = atol(strtok(NULL, ","));
      pf->files[pf->nr_files].read_format = atol(strtok(NULL, ","));
      ++pf->nr_files;
    }

  free(line);
  fclose(f_summary);
  return 0;
}

int
spe_parse__parse(struct spe_parse__conf * conf)
{
  struct parse_files pf;
  check_return(read_summary(conf, &pf), "Error while reading summary file.\n");

  int i = 0;
  int tot_lines = 0;
  struct parse_file * pfp;
  for (; i < pf.nr_files; ++i)
    {
      pfp = &pf.files[i];

      char name_buff[FILENAME_MAX];
      unsigned char buff[SAMPLE_BUFF_SIZE];

      sprintf(name_buff, "%s", pfp->filename);
      FILE * f_bin = fopen(name_buff, "r");
      check_notnull(f_bin,
          "Failed to open binary file %s for group: %d, event: %d\n", name_buff,
          pfp->g_idx, pfp->e_idx);

      sprintf(name_buff, "%s/%s_%d_%d.csv", conf->input_path,
          conf->output_preffix, pfp->g_idx, pfp->e_idx);
      scm_output so;
      output_init(&so, O_CSV, name_buff, conf->input_path);

      int offset = 0;
      int read = 0;
      int nr_records_lost = 0;

      while (!feof(f_bin))
        {
          int toread = SAMPLE_BUFF_SIZE - offset;
          read = fread(buff + offset, 1, toread, f_bin);
          if(read < toread)
            {
              if(!feof(f_bin))
                REPORTERROR("Error %s while reading file %s", ferror(f_bin), pfp->filename);
          }
          int wrote = print_sample_hr(offset + read, buff, pfp->sample_type,
              pfp->read_format, &so, 0, &nr_records_lost);
          assert(wrote <= SAMPLE_BUFF_SIZE);
          if(wrote < 0){
              REPORTERROR("Invalid record found.\n");
          }
          if (wrote < offset + read)
            {
              int remaining_bytes = offset + read - wrote;
              memmove(buff, buff + wrote, remaining_bytes);
              offset = remaining_bytes;
            }
          else
            {
              offset = 0;
            }
        }
      if (nr_records_lost > 0)
        {
          REPORTERROR("Total records lost: %lld\n", nr_records_lost);
        }

      fclose(f_bin);
      int nr_lines = output_destroy(&so);
      tot_lines += nr_lines;
      printf("Printed %d lines\n", nr_lines);
    }
  printf("Total printed %d lines\n", tot_lines);
  return 0;
}
