/*
 * spe_parse.h
 *
 *  Created on: Apr 5, 2013
 *      Author: papirrin
 */

#ifndef SPE_PARSE_H_
#define SPE_PARSE_H_

#include "spe_internal.h"

struct spe_parse__conf{
  char input_path [FILENAME_MAX];
  char input_summ_fn [FILENAME_MAX];
  char output_preffix [FILENAME_MAX];
};

int spe_parse__parse(struct spe_parse__conf * conf);
#endif /* SPE_PARSE_H_ */
