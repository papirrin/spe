/*
 * control_page.c
 *
 *  Created on: Mar 26, 2013
 *      Author: papirrin
 */

#define _GNU_SOURCE 1

#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include "spe_internal.h"
#include "output_manager.h"

#include <linux/perf_event.h>

int
print_misc_field(int misc, char * buff)
{
  char * tmp_str;
  switch (misc & PERF_RECORD_MISC_CPUMODE_MASK)
    {
  case PERF_RECORD_MISC_CPUMODE_UNKNOWN:
    tmp_str = "PERF_RECORD_MISC_CPUMODE_UNKNOWN";
    break;
  case PERF_RECORD_MISC_KERNEL:
    tmp_str = "PERF_RECORD_MISC_KERNEL";
    break;
  case PERF_RECORD_MISC_USER:
    tmp_str = "PERF_RECORD_MISC_USER";
    break;
  case PERF_RECORD_MISC_HYPERVISOR:
    tmp_str = "PERF_RECORD_MISC_HYPERVISOR";
    break;
  case PERF_RECORD_MISC_GUEST_KERNEL:
    tmp_str = "PERF_RECORD_MISC_GUEST_KERNEL";
    break;
  case PERF_RECORD_MISC_GUEST_USER:
    tmp_str = "PERF_RECORD_MISC_GUEST_USER";
    break;
  default:
    tmp_str = "Unknown!";
    REPORTERROR("Unknown misc type.\n");
    return (-1);
    break;
    }
  sprintf(buff, "%s", tmp_str);
  return 0;
}

int
print_record_type(int event_type, char * buff)
{
  char * tmp_str;
  switch (event_type)
    {
  case 1:
    tmp_str = "PERF_RECORD_MMAP";
    break;
  case 2:
    tmp_str = "PERF_RECORD_LOST";
    break;
  case 3:
    tmp_str = "PERF_RECORD_COMM";
    break;
  case 4:
    tmp_str = "PERF_RECORD_EXIT";
    break;
  case 5:
    tmp_str = "PERF_RECORD_THROTTLE";
    break;
  case 6:
    tmp_str = "PERF_RECORD_UNTHROTTLE";
    break;
  case 7:
    tmp_str = "PERF_RECORD_FORK";
    break;
  case 8:
    tmp_str = "PERF_RECORD_READ";
    break;
  case 9:
    tmp_str = "PERF_RECORD_SAMPLE";
    break;
  default:
    tmp_str = "UNKNOWN";
    REPORTERROR("Unknown record type.\n");
    return -1;
    break;
    }
  sprintf(buff, "%s", tmp_str);
  return 0;
}

int
handle_struct_read_format(unsigned char *sample, int read_format,
    scm_output * so)
{

  long long int offset = 0, i, quiet = 0;

  if (read_format & PERF_FORMAT_GROUP)
    {
      long long nr, time_enabled, time_running;

      memcpy(&nr, &sample[offset], sizeof(long long));
      output_field(so, "read_format_number", "%lld", nr);
      offset += 8;

      if (read_format & PERF_FORMAT_TOTAL_TIME_ENABLED)
        {
          memcpy(&time_enabled, &sample[offset], sizeof(long long));
          output_field(so, "read_format_time_enabled", "%lld", time_enabled);
          offset += 8;
        }
      if (read_format & PERF_FORMAT_TOTAL_TIME_RUNNING)
        {
          memcpy(&time_running, &sample[offset], sizeof(long long));
          output_field(so, "read_format_time_running", "%lld", time_running);
          offset += 8;
        }

      for (i = 0; i < nr; i++)
        {
          char buff[TEXT_BUFFS_SIZE];
          long long value, id;
          memcpy(&value, &sample[offset], sizeof(long long));

          sprintf(buff, "read_format_event_%lld_value", i);
          output_field(so, buff, "%lld", value);
          offset += 8;

          if (read_format & PERF_FORMAT_ID)
            {
              memcpy(&id, &sample[offset], sizeof(long long));
              sprintf(buff, "read_format_event_%lld_id", i);
              output_field(so, buff, "%lld", id);
              offset += 8;
            }
        }
    }
  else
    {
      long long value, time_enabled, time_running, id;
      memcpy(&value, &sample[offset], sizeof(long long));
      if (!quiet)
        printf("\t\tValue: %lld ", value);
      offset += 8;

      if (read_format & PERF_FORMAT_TOTAL_TIME_ENABLED)
        {
          memcpy(&time_enabled, &sample[offset], sizeof(long long));
          if (!quiet)
            printf("enabled: %lld ", time_enabled);
          offset += 8;
        }
      if (read_format & PERF_FORMAT_TOTAL_TIME_RUNNING)
        {
          memcpy(&time_running, &sample[offset], sizeof(long long));
          if (!quiet)
            printf("running: %lld ", time_running);
          offset += 8;
        }
      if (read_format & PERF_FORMAT_ID)
        {
          memcpy(&id, &sample[offset], sizeof(long long));
          if (!quiet)
            printf("id: %lld ", id);
          offset += 8;
        }
      if (!quiet)
        printf("\n");
    }

  return offset;
}

int
print_sample_hr(int size, unsigned char *data, int sample_type, int read_format,
    scm_output * so, int print_text_fields, int * nr_records_lost)
{
  char buff[TEXT_BUFFS_SIZE];
  struct perf_event_header *event;

  int offset = 0, i, ret, output_line;
  while (offset < size)
    {
      output_line = 0;
      event = (struct perf_event_header *) &data[offset];
      if (offset + event->size > size)
        {
          break;
        }
      ret = print_record_type(event->type, buff);
      if (ret != 0)
        {
          fprintf(stderr, "Invalid record_type:%d\n", event->type);
          return ret;
        }

      offset += sizeof(struct perf_event_header);
      switch (event->type)
        {

      case PERF_RECORD_LOST:
        {
          long long id, lost;
          memcpy(&id, &data[offset], sizeof(long long));

          //output_field(so, "ID", "%lld", id);
          offset += 8;
          memcpy(&lost, &data[offset], sizeof(long long));

          //output_field(so, "LOST", "%lld", lost);

          (*nr_records_lost) += lost;
          REPORTERROR("Record Lost\t ID:%lld, Nr:%lld\n", id, lost);

          offset += 8;
        }
        break;

      case PERF_RECORD_SAMPLE:
        output_line =1;
        // Start printing record information
        output_field(so, "record_type", "%d", event->type);
        if (print_text_fields)
          output_field(so, "record_type_txt", "%s", buff);

        ret = print_misc_field(event->misc, buff);
        if (ret != 0)
          {
            fprintf(stderr, "Invalid event_misc:%d\n", event->type);
            return ret;
          }
        output_field(so, "event_misc", "%d", event->misc);
        if (print_text_fields)
          output_field(so, "event_misc_txt", "%s", buff);

        output_field(so, "precise_ip", "%s",
            (event->misc & PERF_RECORD_MISC_EXACT_IP)?"1":"0");

            output_field(so, "misc_ext_reserved", "%s",
                (event->misc & PERF_RECORD_MISC_EXT_RESERVED)?"1":"0");

            if (sample_type & PERF_SAMPLE_IP)
              {
                long long ip;
                memcpy(&ip, &data[offset], sizeof(long long));

                output_field(so, "ip", "%lld", ip);

                offset += 8;
              }
            if (sample_type & PERF_SAMPLE_TID)
              {
                int pid, tid;
                memcpy(&pid, &data[offset], sizeof(int));
                memcpy(&tid, &data[offset + 4], sizeof(int));

                output_field(so, "pid", "%lld", pid);
                output_field(so, "tid", "%lld", tid);

                offset += 8;
              }
            if (sample_type & PERF_SAMPLE_TIME)
              {
                long long time;
                memcpy(&time, &data[offset], sizeof(long long));

                output_field(so, "sample_time", "%lld", time);
                offset += 8;
              }
            if (sample_type & PERF_SAMPLE_ADDR)
              {
                long long addr;
                memcpy(&addr, &data[offset], sizeof(long long));

                output_field(so, "sample_addr", "%lld", addr);
                offset += 8;
              }
            if (sample_type & PERF_SAMPLE_ID)
              {
                long long sample_id;
                memcpy(&sample_id, &data[offset], sizeof(long long));
                output_field(so, "sample_id", "%lld", sample_id);
                offset += 8;
              }
            if (sample_type & PERF_SAMPLE_STREAM_ID)
              {
                long long sample_stream_id;
                memcpy(&sample_stream_id, &data[offset], sizeof(long long));
                output_field(so, "sample_stream_id", "%lld", sample_stream_id);

                offset += 8;
              }
            if (sample_type & PERF_SAMPLE_CPU)
              {
                int cpu, res;
                memcpy(&cpu, &data[offset], sizeof(int));
                memcpy(&res, &data[offset + 4], sizeof(int));

                output_field(so, "cpu", "%d", cpu);
                output_field(so, "res", "%d", res);
                offset += 8;
              }
            if (sample_type & PERF_SAMPLE_PERIOD)
              {
                long long period;
                memcpy(&period, &data[offset], sizeof(long long));

                output_field(so, "period", "%lld", period);
                offset += 8;
              }
            if (sample_type & PERF_SAMPLE_READ)
              {
                int length;

                length = handle_struct_read_format(&data[offset], read_format, so);

                /* if (length>=0) */offset += length;
              }
            if (sample_type & PERF_SAMPLE_CALLCHAIN)
              {
                long long nr, ip;
                memcpy(&nr, &data[offset], sizeof(long long));

                printf("\tcallchain length: %lld\n", nr);
                offset += 8;

                for (i = 0; i < nr; i++)
                  {
                    memcpy(&ip, &data[offset], sizeof(long long));

                    printf("\t\t ip[%d]: %llx\n", i, ip);
                    offset += 8;
                  }
              }
            if (sample_type & PERF_SAMPLE_RAW)
              {
                int size;

                memcpy(&size, &data[offset], sizeof(int));

                output_field(so, "raw_length_size", "%d", size);
                offset += 4;

                printf("\t\t");

                for (i = 0; i < size; i++)
                  {

                    printf("%d ", data[offset]);
                    offset += 1;
                  }

                printf("\n");
              }

            if (sample_type & PERF_SAMPLE_BRANCH_STACK)
              {
                long long bnr;
                memcpy(&bnr, &data[offset], sizeof(long long));

                printf("\tbranch_stack entries: %lld\n", bnr);
                offset += 8;

                for (i = 0; i < bnr; i++)
                  {
                    long long from, to, flags;
                    memcpy(&from, &data[offset], sizeof(long long));
                    offset += 8;
                    memcpy(&to, &data[offset], sizeof(long long));
                    offset += 8;
                    memcpy(&flags, &data[offset], sizeof(long long));
                    offset += 8;

                    printf("\t\t lbr[%d]: %llx %llx %llx\n", i, from, to, flags);

                  }
              }
            break;

      case PERF_RECORD_THROTTLE:
        SUBDBG("PERF_RECORD_THROTTLE found and was ignored.\n");
        offset += sizeof(struct perf_event_header) + 4+4+8;
        break;
      case PERF_RECORD_UNTHROTTLE:
        SUBDBG("PERF_RECORD_UNTHROTTLE found and was ignored.\n");
        offset += sizeof(struct perf_event_header) + 4+4+8;
        break;
      default:

            REPORTERROR("spe_parse: Unknown Event type %d\n", event->type);
            break;
          }
      if(output_line){
          output_endl(so);
      }
    }
  return offset;
}

