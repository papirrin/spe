/*
 * output_manager.h
 *
 *  Created on: Mar 26, 2013
 *      Author: papirrin
 */

#ifndef OUTPUT_MANAGER_H_
#define OUTPUT_MANAGER_H_

#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>

typedef enum {
  O_CSV, O_HR
} output_type;

typedef struct {
  FILE * o_names;
  FILE * o_values;
  output_type o_type;
  int curr_line;
  int curr_field_idx;
} scm_output;

int output_init(scm_output * so, output_type ot, char * filename, char * path) {
  FILE * of, *auxf;
  char buff[FILENAME_MAX];
  if (filename != NULL )
  {
    of = fopen(filename, "w+");
    check_notnull(of, "Error opening output file %s", filename);
  }
  else{
    of = stdout;
  }
  so->o_type = ot;
  so->curr_line = 0;
  so->curr_field_idx =0;

  switch (ot) {
    case O_HR:
      so->o_names = of;
      so->o_values = of;
      break;
    case O_CSV:
      sprintf(buff, "%s/aux_file_tmp.tmp", path);
      auxf = fopen(buff, "w+");
      check_notnull(auxf, "Error opening temporal file %s", buff);
      so->o_names = of;
      so->o_values = auxf;
      break;
    default:
      return -1;
  }
  return 0;
}

int is_std_stream(FILE * o) {
  if (o == stdout || o == stderr) {
      return 1;
  }
  return 0;
}

void
append(FILE *head, FILE *tail)
{
  char buf[BUFSIZ];
  size_t n;
  while ((n = fread(buf, 1, sizeof buf, tail)) > 0)
    if (fwrite(buf, 1, n, head) != n)
      abort();
  if (ferror(tail))
    abort();
}

int output_destroy(scm_output * o) {
  check_notneg(fflush(o->o_values), "Error flushing values stream.\n");
  if(o->o_names != o->o_values){
      check_notneg(fflush(o->o_names), "Error flushing names stream.\n");
  }
  // Append csv values to csv names file
  if(o->o_type == O_CSV){
      rewind(o->o_values);
      append(o->o_names, o->o_values);
  }
  //
  if (o->o_names != NULL && !is_std_stream(o->o_names)) {
      fclose(o->o_names);
  }
  if (o->o_names != o->o_values && o->o_values != NULL
      && !is_std_stream(o->o_values)) {
      fclose(o->o_values);
  }
  o->o_names = NULL;
  o->o_values = NULL;
  return o->curr_line;
}


void output_field(scm_output * scm_o, const char * f_name,
		const char * value_format, ...) {

	va_list ap;
	va_start(ap, value_format);

	switch (scm_o->o_type) {
	case O_HR:
		assert(scm_o->o_names == scm_o->o_values);
		fprintf(scm_o->o_names, "%s : ", f_name);
		vfprintf(scm_o->o_names, value_format, ap);
		fprintf(scm_o->o_names, "\n");
		break;
	case O_CSV:
		if(scm_o->curr_line < 1){
			if(scm_o->curr_field_idx > 0)
						fprintf(scm_o->o_names, ",");
			fprintf(scm_o->o_names, "%s", f_name);
		}
		if(scm_o->curr_field_idx > 0)
					fprintf(scm_o->o_values, ",");
		vfprintf(scm_o->o_values, value_format, ap);
		break;
	default:
		REPORTERROR("Unsupported output type.\n");
	}
	va_end(ap);

	scm_o->curr_field_idx++;

}

void output_endl(scm_output * o) {
	if(o->o_type == O_CSV && o->curr_line < 1){
		fprintf(o->o_names, "\n");
	}
	fprintf(o->o_values, "\n");
	o->curr_line++;
	o->curr_field_idx = 0;
}

#endif /* OUTPUT_MANAGER_H_ */
