Simple Performance Events

./spe -p f:100000,f:100001,f:10002,f:10004 -a 0:3 -v ls -al

./spe -p f:3000000,f:3300000 -a 0:3 -v ../../adaptive-sampling-experiments/loads/tasks_cmd s:500000,f:1000000,d:500000,a:500000

Multiplex example:

./spe -p f:1000,f:1000,f:1000,f:1000,f:1000,f:1000,f:1000,f:1000 -c 18 -a 0:3 -d /data/dest/dir/ -v sleep 10

-c 18 = SPC_CTL_MULTIPLEX8
